﻿<?php
require("./functions.php");
$url = $_SERVER["REQUEST_URI"];

include_once("header.php");
?>

<div id="container">
<div id="containerWrap"></div>
	<div class="performanceBox">
        <!-- NEWS_______________________________________________________ -->

    <div class="performance_news">
            <div class="performance_title">
                <h1>NEWS</h1>
            </div>
            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2017.8.2                        </div>
                        <div class="performance_content_text">
                            株式会社スマートライフとの資本業務提携のお知らせ <br/> 	<a href="release/170802_SmartLife.pdf">PRESS RELEASE</a><br>                        </div>
                    </div>
                </div>
            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2017.7.4                        </div>
                        <div class="performance_content_text">
                            採用ページをリニューアルいたしました。                        </div>
                    </div>
                </div>
                <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2017.6.26                        </div>
                        <div class="performance_content_text">
                            中高生向け授業動画サービス<a href="http://manavie.jp/" target="_blank">「MANAVIE」β版</a>をリリースしました！                        </div>
                    </div>
                </div>
                <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2017.6.19                        </div>
                        <div class="performance_content_text">
                            <a href="http://smartschool.oceanize.co.jp/azabu/plan/summer_school_2017.pdf">SmartSchool麻布本校主催　小学生向け「社会とつながるサマースクール」を実施します！</a>                       </div>
                    </div>
                </div>
                 <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2017.5.1                        </div>
                        <div class="performance_content_text">
                            <a href="http://smartschool.oceanize.co.jp/azabu/" target="_blank">自社運営教室「SmartSchool 麻布本校」開校！</a>               </div>
                    </div>
                </div>
            	 <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2017.4.13                        </div>
                        <div class="performance_content_text">
                            北九州市立大学・北方キャンパスにタダコピ登場！                        </div>
                    </div>
                </div>
            	 <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2017.4.13                     </div>
                        <div class="performance_content_text">
                            北九州市立大学・ひびきのキャンパスにタダコピ登場！                        </div>
                    </div>
                </div>
            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2017.3.24                        </div>
                        <div class="performance_content_text">
                            オフィスを移転いたしました。                        </div>
                    </div>
                </div>
            	 <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2017.3.3                        </div>
                        <div class="performance_content_text">
                            大阪教育大学・柏原キャンパスにタダコピ登場！                        </div>
                    </div>
                </div>
            	<div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2016.8.1                        </div>
                        <div class="performance_content_text">
                            写真共有アプリ『PICON』事業譲受のお知らせ <br/> 	<a href="release/160801_PICON.pdf">PRESS RELEASE</a><br>                        </div>
                    </div>
                </div>
                <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2016.7.19                        </div>
                        <div class="performance_content_text">
                            大東文化大学・東松山キャンパスにタダコピ登場！                        </div>
                    </div>
                </div>
                <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2016.6.24                        </div>
                        <div class="performance_content_text">
                            岡山理科大学にタダコピ登場！                        </div>
                    </div>
                </div>
            	<div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2016.4.22                        </div>
                        <div class="performance_content_text">
                            「大学生のお金の教科書2016 ～大学生の夢と現実とお金の話。」10万部を全国の大学（57大学74キャンパス）で配布！<br/> 	<a href="release/160422_UNISA.pdf">PRESS RELEASE</a><br>                        </div>
                    </div>
                </div>
		<div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2015.6.22                        </div>
                        <div class="performance_content_text">
                            オフィスを移転いたしました。                        </div>
                    </div>
                </div>
		<div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2015.3.30                        </div>
                        <div class="performance_content_text">
                            帝京科学大学・千住キャンパスにタダコピ登場！                        </div>
                    </div>
                </div>
 		<div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2015.3.23                        </div>
                        <div class="performance_content_text">
                            東京農工大学・府中キャンパスにタダコピ登場！                        </div>
                    </div>
                </div>

		<div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2015.3.20                        </div>
                        <div class="performance_content_text">
                            ホームページをリニューアルしました。                        </div>
                    </div>
                </div>

 		<div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2015.3.18                        </div>
                        <div class="performance_content_text">
                            近畿大学にタダコピ登場！                        </div>
                    </div>
                </div>

 		<div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2015.3.10                        </div>
                        <div class="performance_content_text">
                            専修大学・生田キャンパスにタダコピ登場！                        </div>
                    </div>
                </div>

 		<div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2015.3.6                        </div>
                        <div class="performance_content_text">
                            中央大学・後楽園キャンパスにタダコピ登場！                        </div>
                    </div>
                </div>
<!--<?php /*
                <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2015.1.22                        </div>
                        <div class="performance_content_text">
                             <a href="./150122_UNISA.php">大学生への金融リテラシー普及プロジェクト「UNISA」始動！</a>                        </div>
                    </div>
                </div>
                */ ?>-->
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2014.10.1                        </div>
                        <div class="performance_content_text">
                            当社のロゴをリニューアルしました。                        </div>
                    </div>
                </div>
                <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2014.9.19                        </div>
                        <div class="performance_content_text">
                            武蔵大学・江古田キャンパスにタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2014.9.17                        </div>
                        <div class="performance_content_text">
                            <a target="_blank" href="https://toretan.jp/">東大・早稲田・慶應・明治・青山・立教・中央・法政の8大学限定で、<br> リニューアルしたトレタン！サービスを開始しました。</a>                         </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2014.4.8                        </div>
                        <div class="performance_content_text">
                             <a href="news140408.html">スマホで授業を“シェア”できるグループカメラアプリ「トレる単位(トレタン)α版」が提供開始！</a>                         </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2014.3.13                        </div>
                        <div class="performance_content_text">
                            愛知学院大学・名城公園キャンパスにタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2014.2.28                        </div>
                        <div class="performance_content_text">
                            学習院女子大学にタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2014.1.14                        </div>
                        <div class="performance_content_text">
                            日本女子体育大学にタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2013.12.17                        </div>
                        <div class="performance_content_text">
                             <a href="release/131217_CervicalCancerPR.pdf">子宮頸がん啓発番組をタダchにて放送開始！</a>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2013.11.12                        </div>
                        <div class="performance_content_text">
                            愛知学院大学・日進キャンパスにタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2013.10.9                        </div>
                        <div class="performance_content_text">
                            國學院大学・渋谷キャンパスにタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2013.9.27                        </div>
                        <div class="performance_content_text">
                            國學院大学・たまプラーザキャンパスにタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2013.7.11                        </div>
                        <div class="performance_content_text">
                             <a href="news130711.html">参院選「ブラックジャックによろしく」がタダコピジャックで大学生に投票よろしく！！</a>                         </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2013.7.9                        </div>
                        <div class="performance_content_text">
                             <a href="news130709.html">大学生向け無料アプリ「タダコピアプリ」の<br>アンケート回答者数&#12288;延べ10,000人突破！</a>                         </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2013.6.25                        </div>
                        <div class="performance_content_text">
                            城西国際大学・紀尾井町キャンパスにタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2013.6.24                        </div>
                        <div class="performance_content_text">
                            作新学院大学にタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2013.6.21                        </div>
                        <div class="performance_content_text">
                            日本体育大学・健志台キャンパスにタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2013.6.20                        </div>
                        <div class="performance_content_text">
                            日本体育大学・世田谷キャンパスにタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2013.5.29                        </div>
                        <div class="performance_content_text">
                            桐蔭横浜大学にタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2013.5.10                        </div>
                        <div class="performance_content_text">
                            国士舘大学・世田谷キャンパスにタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2013.2.28                        </div>
                        <div class="performance_content_text">
                             <a href="news130228.html">テレビ東京と大学生向け情報配信サービスを開始！<br>情報提供サイネージ『タダコピチャンネル（呼称：タダｃｈ(チャン)）』</a>                         </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2012.11.28                        </div>
                        <div class="performance_content_text">
                             <a href="news121128.html">電子ブックサービスの新革命！大学生向け電子ブック閲覧サービス<br>「Rucksack」が11月28日より始まる！</a>                         </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2012.11.26                        </div>
                        <div class="performance_content_text">
                            中京大学・豊田キャンパスにタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2012.11.22                        </div>
                        <div class="performance_content_text">
                            茨城大学・水戸キャンパスにタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2012.10.29                        </div>
                        <div class="performance_content_text">
                            畿央大学にタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2012.9.27                        </div>
                        <div class="performance_content_text">
                            広島市立大学にタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2012.7.9                        </div>
                        <div class="performance_content_text">
                            神戸女学院大学にタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2012.7.1                        </div>
                        <div class="performance_content_text">
                            四国大学にタダコピ登場！祝・徳島初上陸！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2012.6.26                        </div>
                        <div class="performance_content_text">
                            白鴎大学・本キャンパスにタダコピ登場！祝・栃木初上陸！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2012.6.22                        </div>
                        <div class="performance_content_text">
                            新潟青陵大学にタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2012.6.21                        </div>
                        <div class="performance_content_text">
                            名古屋学芸大学にタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2012.6.18                        </div>
                        <div class="performance_content_text">
                            日本女子大学・西生田キャンパスにタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2012.5.23                        </div>
                        <div class="performance_content_text">
                            流通科学大学にタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2012.5.22                        </div>
                        <div class="performance_content_text">
                            名古屋芸術大学・西キャンパスにタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2012.5.21                        </div>
                        <div class="performance_content_text">
                            高崎経済大学にタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2012.5.16                        </div>
                        <div class="performance_content_text">
                            共愛学園前橋国際大学にタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2012.5.15                        </div>
                        <div class="performance_content_text">
                            いわき明星大学にタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2012.4.13                        </div>
                        <div class="performance_content_text">
                            松山大学にタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2012.4.12                        </div>
                        <div class="performance_content_text">
                            <a href="media120412.html">各ニュースサイトでタダスマが紹介されました。</a>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2012.3.16                        </div>
                        <div class="performance_content_text">
                            常葉学園大学・静岡キャンパスにタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2012.3.15                        </div>
                        <div class="performance_content_text">
                            兵庫大学にタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2012.3.14                        </div>
                        <div class="performance_content_text">
                            広島工業大学にタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2012.3.2                        </div>
                        <div class="performance_content_text">
                            女子栄養大学・坂戸キャンパスにタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2012.2.13                        </div>
                        <div class="performance_content_text">
                            文教大学・越谷キャンパスにタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2012.1.31                        </div>
                        <div class="performance_content_text">
                            西日本工業大学・おばせキャンパスにタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2012.1.30                        </div>
                        <div class="performance_content_text">
                            名古屋外国語大学・日進キャンパスにタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2012.1.17                        </div>
                        <div class="performance_content_text">
                            神戸女子大学・須磨キャンパスにタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2012.1.11                        </div>
                        <div class="performance_content_text">
                            津田塾大学にタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2011.12.27                        </div>
                        <div class="performance_content_text">
                            金沢学院大学にタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2011.12.26                        </div>
                        <div class="performance_content_text">
                            目白大学・新宿キャンパスにタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2011.12.21                        </div>
                        <div class="performance_content_text">
                            神戸学院大学・ポートアイランドキャンパスにタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2011.12.20                        </div>
                        <div class="performance_content_text">
                            城西国際大学・千葉東金キャンパスにタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2011.12.16                        </div>
                        <div class="performance_content_text">
                            名古屋経済大学・犬山キャンパスにタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2011.12.15                        </div>
                        <div class="performance_content_text">
                            文教大学・湘南キャンパスにタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2011.12.7                        </div>
                        <div class="performance_content_text">
                            桃山学院大学・和泉キャンパスにタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2011.11.30                        </div>
                        <div class="performance_content_text">
                            麗澤大学にタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2011.11.29                        </div>
                        <div class="performance_content_text">
                            <a href="news111129.html">就活生とスマートフォンに関する意識比較調査を発表</a>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2011.11.25                        </div>
                        <div class="performance_content_text">
                            相模女子大学にタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2011.11.16                        </div>
                        <div class="performance_content_text">
                            倉敷芸術科学大学にタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2011.11.15                        </div>
                        <div class="performance_content_text">
                            東京女子体育大学にタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2011.11.7                        </div>
                        <div class="performance_content_text">
                             <a href="news111107.html">           北京（清華大・人民大）で11月15日から『タダコピ』開始！</a>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2011.10.31                        </div>
                        <div class="performance_content_text">
                            岩手県立大学・滝沢キャンパスにタダコピ登場！祝・東北初上陸！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2011.10.28                        </div>
                        <div class="performance_content_text">
                            東京造形大学にタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2011.10.26                        </div>
                        <div class="performance_content_text">
                            九州保健福祉大学にタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2011.10.25                        </div>
                        <div class="performance_content_text">
                            <a href="news111025.html">「 CareerRec(キャリアレック) 」をβオープン</a>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2011.10.24                        </div>
                        <div class="performance_content_text">
                            久留米大学にタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2011.10.20                        </div>
                        <div class="performance_content_text">
                            高知工科大学にタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2011.10.14                        </div>
                        <div class="performance_content_text">
                            都留文科大学にタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2011.9.30                        </div>
                        <div class="performance_content_text">
                            崇城大学にタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2011.9.29                        </div>
                        <div class="performance_content_text">
                            学習院大学にタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2011.9.28                        </div>
                        <div class="performance_content_text">
                            岐阜聖徳学園大学・岐阜キャンパスにタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2011.9.15                        </div>
                        <div class="performance_content_text">
                            大阪経済法科大学にタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2011.9.14                        </div>
                        <div class="performance_content_text">
                            高崎健康福祉大学にタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2011.9.13                        </div>
                        <div class="performance_content_text">
                            京都学園大学にタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2011.9.9                        </div>
                        <div class="performance_content_text">
                            関東学院大学・小田原キャンパスにタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2011.9.8                        </div>
                        <div class="performance_content_text">
                            北里大学・相模大野キャンパスにタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2011.9.7                        </div>
                        <div class="performance_content_text">
                            多摩美術大学・八王子キャンパスにタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2011.9.6                        </div>
                        <div class="performance_content_text">
                            白百合女子大学・仙川キャンパスにタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2011.9.5                        </div>
                        <div class="performance_content_text">
                            聖心女子大学・広尾キャンパスにタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2011.7.19                        </div>
                        <div class="performance_content_text">
                            立正大学・大崎キャンパスにタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2011.7.15                        </div>
                        <div class="performance_content_text">
                            明海大学・浦安キャンパスにタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2011.7.14                        </div>
                        <div class="performance_content_text">
                            関東学院大学・金沢八景キャンパス・人間環境学部にタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2011.7.13                        </div>
                        <div class="performance_content_text">
                            女子美術大学にタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2011.7.12                        </div>
                        <div class="performance_content_text">
                            江戸川大学にタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2011.7.1                        </div>
                        <div class="performance_content_text">
                            駿河台大学にタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2011.6.16                        </div>
                        <div class="performance_content_text">
                             <a href="news110616.html">           上海子会社事務所移転のお知らせ</a>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2011.5.25                        </div>
                        <div class="performance_content_text">
                             <a href="news110525.html">           FUJIWARA Corporationとの業務提携に関するお知らせ</a>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2011.5.12                        </div>
                        <div class="performance_content_text">
                            鹿児島国際大学にタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2011.5.10                        </div>
                        <div class="performance_content_text">
                                                    </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2011.4.25                        </div>
                        <div class="performance_content_text">
                            日本経済大学・福岡キャンパスにタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2011.4.15                        </div>
                        <div class="performance_content_text">
                            志學館大学にタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                                      <a href="news2010.html">もっと見る...</a>                        </div>
                        <div class="performance_content_text">
                                                    </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                                                    </div>
                        <div class="performance_content_text">
                                           <a href="media.html">もっと見る...</a>                                     </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2010.12.15                        </div>
                        <div class="performance_content_text">
                             椙山女学園大学にタダコピ登場！<br>                         </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2010.12.8                        </div>
                        <div class="performance_content_text">
                             <a href="news101208.html">立教大学ミスコンとタダコピのコラボ企画</a><br>                         </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2010.10.19                        </div>
                        <div class="performance_content_text">
                             名古屋学院大学にタダコピ登場！<br>                         </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2010.9.17                        </div>
                        <div class="performance_content_text">
                             東京女子大学にタダコピ登場！<br>                         </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2010.9.15                        </div>
                        <div class="performance_content_text">
                             浜松大学にタダコピ登場！<br>                         </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2010.9.8                        </div>
                        <div class="performance_content_text">
                             琉球大学にタダコピ登場！<br>                         </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2010.9.2                        </div>
                        <div class="performance_content_text">
                             <a href="news100902.html">「子宮頸がん啓発ツールバー」を新たに開発。全国の大学に導入へ</a>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2010.8.9                        </div>
                        <div class="performance_content_text">
                             <a href="news100809.html"> 中国版『タダコピ』 上海でエリア拡大し、北京進出も視野に！<br> </a>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2010.7.16                        </div>
                        <div class="performance_content_text">
                             関東学院大学にタダコピ登場！<br>                         </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2010.6.30                        </div>
                        <div class="performance_content_text">
                             北里大学にタダコピ登場！<br>                         </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2010.6.26                        </div>
                        <div class="performance_content_text">
                             杏林大学にタダコピ登場！<br>                         </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2010.6.17                        </div>
                        <div class="performance_content_text">
                             明星大学にタダコピ登場！<br>                         </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2010.4.19                        </div>
                        <div class="performance_content_text">
                             <a target="_blank" href="release/0419samurai.pdf">【NEWS  RELEASE】"サムライバックパッカープロジェクト"始動<br> </a>※PDFファイル                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2009.11.16                        </div>
                        <div class="performance_content_text">
                             <a href="news091116.html">インフルエンザ予防啓発&#12288;タダコピで訴求！</a>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2009.10.16                        </div>
                        <div class="performance_content_text">
                             <a href="news091016.html">無料コピーサービス『タダコピ』遂に上海上陸！！</a>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2009.09.18                        </div>
                        <div class="performance_content_text">
                             <a href="news090918.html">世界初！最強の無料コピーサービス、キャンパス内で始まる！！ </a>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2009.09.17                        </div>
                        <div class="performance_content_text">
                             <a href="news090917.html">10月1日はコーヒーの日 9月30日ミスキャンパス候補17人が渋谷に大集合！</a>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2009.09.15                        </div>
                        <div class="performance_content_text">
                             <a href="news090915b.html">堺市長選挙 タダコピでPR！ 9月15日（火）から大阪府立大学で若年層の投票を呼びかけ</a>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2009.09.01                        </div>
                        <div class="performance_content_text">
                            9月1日から第８回『<a target="_new" href="http://tgks.vsn.jp/">学生起業家選手権</a>』の募集がスタートしました！<br>弊社代表  菅澤が審査員として参加します。                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2009.08.19                        </div>
                        <div class="performance_content_text">
                             <a href="news090819.html">衆議院議員選挙 １都３県がタダコピで合同PR！若年層の投票を呼びかけ</a>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2009.08.06                        </div>
                        <div class="performance_content_text">
                             <a href="news090806.html">女子トイレで学ぶ２ 〜『タダコピ green RING』 子宮頸がん啓発活動 〜</a>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2009.07.15                        </div>
                        <div class="performance_content_text">
                             <a href="news090715.html">安田女子大学にてタダコピサービススタート！</a>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2009.06.12                        </div>
                        <div class="performance_content_text">
                             <a href="news090612.html">6月15日から2都県の夏の地方選を連動型紙面でPR</a>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2009.05.13                        </div>
                        <div class="performance_content_text">
                             <a href="news090513_1.html">5月15日より「吉備国際大学」にタダコピが登場！</a>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2009.04.15                        </div>
                        <div class="performance_content_text">
                             <a href="news090415.html">名古屋市長選挙のPRを『タダコピ』でスタート</a>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2009.04.01                        </div>
                        <div class="performance_content_text">
                             <a href="news090401_1.html">代表取締役および取締役の異動に関するお知らせ</a>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2009.02.17                        </div>
                        <div class="performance_content_text">
                             北海道大学にタダコピ登場                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2009.01.28                        </div>
                        <div class="performance_content_text">
                             <a href="release/090128.pdf">「東京都地域中小企業応援ファンド」対象事業に選定されました。【PDF】</a>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2009.01.27                        </div>
                        <div class="performance_content_text">
                             デザインコンペ実施のお知らせ [参加募集は3月12日をもって、閉め切りました]                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2009.01.09                        </div>
                        <div class="performance_content_text">
                            日本薬科大学にタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2009.01.07                        </div>
                        <div class="performance_content_text">
                            横浜薬科大学にタダコピ登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2008.12.18                        </div>
                        <div class="performance_content_text">
                             <a href="news081218.html">年末年始休暇につきまして</a>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2008.11.28                        </div>
                        <div class="performance_content_text">
                             <a href="release/081128.pdf">【ご挨拶】創業3周年のご挨拶 [PDF]</a>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2008.10.22                        </div>
                        <div class="performance_content_text">
                             <a href="release/081022.pdf">『女子トイレで学ぶ』学生向け新媒体「プライベートメディア」 [PDF]</a>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2008.09.12                        </div>
                        <div class="performance_content_text">
                             <a target="_new" href="http://event.dreamgate.gr.jp/award/">DREAM GATE AWARD 2008</a>にノミネートされました。                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2008.09.11                        </div>
                        <div class="performance_content_text">
                             <a href="release/080916.pdf"> 「岡山理科大学」「川崎医療福祉大学」にタダコピが登場！ [PDF]</a>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2008.08.27                        </div>
                        <div class="performance_content_text">
                             <a href="release/080827.pdf"> 弊社企画協力の『Waseda Collection 2008 in ALTA』！ [PDF]</a>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2008.06.25                        </div>
                        <div class="performance_content_text">
                            <a href="release/080625.pdf">JR京都駅前「キャンパスプラザ京都」にタダコピが登場！ [PDF]</a>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2008.06.16                        </div>
                        <div class="performance_content_text">
                            神戸大学文理農学部キャンパスにタダコピが登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2008.05.19                        </div>
                        <div class="performance_content_text">
                            千葉大学亥鼻キャンパスに「タダコピ」登場！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2008.05.07                        </div>
                        <div class="performance_content_text">
                            <a href="http://www.tadacopy.com/">タダコピ.com</a>をリニューアルオープンしました！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2008.04.03                        </div>
                        <div class="performance_content_text">
                            「タダコピ」サービスインより2周年！                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2008.03.24                        </div>
                        <div class="performance_content_text">
                            <a href="release/080324.pdf"> 『タダコピ』、大阪圏・名古屋圏での導入強化！<br> 3月25日（火）より名古屋工業大学<br> 4月 1日（火）より関西学院大学で開始。<br> 4月末に愛知県立大学・神戸大学（2台目）の導入決定！ [PDF]</a>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2008.02.15                        </div>
                        <div class="performance_content_text">
                            【採用情報】『選考会』に関するコンテンツを追加しました。                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2008.02.01                        </div>
                        <div class="performance_content_text">
                            <a href="release/080201.pdf"> 2月1日(金)より名古屋大学に『タダコピ』導入！ [PDF]</a>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2007.10.25                        </div>
                        <div class="performance_content_text">
                            <a href="release/071025.pdf"> 『タダコピ』、11月1日（木）より大阪大学にも導入！<br> 神戸大学に引き続き、関西圏の国立大学で導入 [PDF]</a>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2007.09.26                        </div>
                        <div class="performance_content_text">
                            <a href="release/070926.pdf"> 『タダコピ』、10月1日（月）より神戸大学に導入！<br> 関西圏の国立大学で初の導入[PDF]</a>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2007.09.12                        </div>
                        <div class="performance_content_text">
                            <a href="release/070912.pdf"> 『タダコピ』、遂に関西本格上陸！9月18日（火）<br> 関西大学・大阪府立大学・京都産業大学に一斉導入</a>                        </div>
                    </div>
                </div>
                    </div>

        <!-- MEDIA_______________________________________________________  -->

        <div class="performance_media">
            <div class="performance_title">
                <h1>MEDIA</h1>
            </div>
            	<div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2017.9.6                        </div>
                        <div class="performance_content_text">
                            大学に広がる無料コピー機「タダコピ」の記事が、<br/>産経新聞、産経ニュースに掲載されました。<br> 	<a rel="nofollow" class="link" target="_blank" href="http://www.sankei.com/life/news/170906/lif1709060014-n1.html">産経ニュース記事</a>(外部サイト)<br>                        </div>
                    </div>
                </div>

                <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2015.3.14                        </div>
                        <div class="performance_content_text">
                            女子大生アイドル日本一決定戦「UNIDOL」の記事が、<br/>朝日新聞、朝日新聞DIGITALに掲載されました。<br> 	<a rel="nofollow" class="link" target="_blank" href="http://www.asahi.com/articles/ASH3B5F22H3BUCVL01R.html">朝日新聞DIGITAL記事</a>(外部サイト)<br>                        </div>
                    </div>
                </div>

                <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2015.3.11                        </div>
                        <div class="performance_content_text">
                            学生の金融リテラシー向上を目指すＳＭＢＣ日興証券様との共同プロジェクト<br/>「ＵＮＩＳＡ（ユニーサ）」がメディアに掲載されました。<br> 	<a rel="nofollow" class="link" target="_blank" href="http://headlines.yahoo.co.jp/hl?a=20150311-00000509-san-bus_all">yahooニュース記事</a>(外部サイト)<br>                        </div>
                    </div>
                </div>

                <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2014.10.8                        </div>
                        <div class="performance_content_text">
                            四月一日企画・シブヤテレビジョンと共同で行った、<br>LINEクリエイタースタンプの人気投票結果が掲載されました。<br> 	<a rel="nofollow" class="link" target="_blank" href="http://www.atpress.ne.jp/view/52106">PressNews</a>(外部サイト)<br>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2014.9.15                        </div>
                        <div class="performance_content_text">
                            日本経済新聞にてタダコピが紹介されました。<br> 	<a rel="nofollow" class="link" target="_blank" href="http://www.nikkei.com/article/DGKDZO77048780T10C14A9TCP000/">無料サービス、学びサポート</a>(外部サイト)<br>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2014.7.28                        </div>
                        <div class="performance_content_text">
                            <a href="media140728.html">「UNIDOL(ユニドル)」が各種ニュースサイトで紹介されました。</a>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2014.6.23                        </div>
                        <div class="performance_content_text">
                            ウェブアンケート作成ツール『SurveyMonkey』（サーベイモンキー）と<br> 2014年4月に大学生911人が対象に共同調査したデータが<br>各WEBメディアに掲載されました。<br> 	<a rel="nofollow" class="link" target="_blank" href="http://getnews.jp/archives/601279">ガジェット通信記事</a>(外部サイト) 	&#12288;&#12288;<a href="media140623.html">その他の掲載メディア</a><br>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2014.5.29                        </div>
                        <div class="performance_content_text">
                            弊社が主催するイベント『UNIDOL 2014 Summer』がyahooニュースをはじめ、<br> 各ニュースサイトにて紹介されました。<br> 	<a rel="nofollow" class="link" target="_blank" href="http://headlines.yahoo.co.jp/hl?a=20140529-00000010-reallive-ent">yahooニュース記事</a>(外部サイト)<br>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2014.4.17                        </div>
                        <div class="performance_content_text">
                            セルフアンケートASP「サーベイモンキー」との業務提携が紹介されました。<br>   <a rel="nofollow" class="link" target="_blank" href="http://markezine.jp/article/detail/19742">MarkeZine記事</a>(外部サイト)                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2014.4.11                        </div>
                        <div class="performance_content_text">
                            <a href="media140411.html">「トレタン」が各ニュースサイトで紹介されました。</a>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2013.7.16                        </div>
                        <div class="performance_content_text">
                            「NEWS ZERO」にて「参議院議員選挙」タダコピ紙面が紹介されました。<br> 	<a rel="nofollow" class="link" target="_blank" href="http://www.ntv.co.jp/zero/ichimen/2013/07/post-242.html">櫻井翔 イチメン！若者よ、選挙に行こう!</a>(外部サイト)<br>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2013.4.3                        </div>
                        <div class="performance_content_text">
                            「Web R25」にてタダコピが紹介されました。<br> 	<a rel="nofollow" class="link" target="_blank" href="http://r25.yahoo.co.jp/fushigi/wxr_detail/?id=20130402-00029031-r25">WebR25『「学生発ヒット商品」の作り方』</a>(外部サイト)<br>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2013.3.7                        </div>
                        <div class="performance_content_text">
                            日経産業新聞にて「タダｃｈ」が紹介されました。                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2013.2.13                        </div>
                        <div class="performance_content_text">
                            「タダコピアプリ」がAppliv(アプリヴ)に掲載されました。<br> 	<a rel="nofollow" class="link" target="_blank" href="http://app-liv.jp/567681014/">Appliv</a>(外部サイト)<br> 	<a rel="nofollow" class="link" target="_blank" href="http://nikkan.app-liv.jp/archives/13581">日刊Appliv</a>(外部サイト)<br>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2012.11.29                        </div>
                        <div class="performance_content_text">
                            <a href="media121129.html">「Rucksack」が各ニュースサイトで紹介されました。</a>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2012.10.29                        </div>
                        <div class="performance_content_text">
                            「サクセス登竜門」にて代表取締役 菅澤聡が紹介されます。（2013年1月放送予定）<br> 	<a rel="nofollow" class="link" target="_blank" href="http://s-touryumon.com/">サクセス登竜門</a>(外部サイト)                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2012.10.09                        </div>
                        <div class="performance_content_text">
                            ウレぴあ総研にて代表取締役 菅澤聡が紹介されました。<br>     <a rel="nofollow" class="link" target="_blank" href="http://ure.pia.co.jp/articles/-/9725">ウレぴあ総研[プレッシャー世代の起業家]<br>『タダコピ』の社長が熱く語る“ビジネスを通して国のあり方を再構築したい”</a>(外部サイト)                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2012.4.12                        </div>
                        <div class="performance_content_text">
                            <a href="media120412.html">各ニュースサイトでタダスマが紹介されました。</a>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2011.5.10                        </div>
                        <div class="performance_content_text">
                             日本テレビ「人生が変わる１分間の深イイ話」の特集にて、<br>タダコピのビジネスモデルが紹介されました。                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2010.8.10                        </div>
                        <div class="performance_content_text">
                             「GetNavi（7/24発売）」の特集にてタダコピのビジネスモデルが紹介されました。                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2010.6.14                        </div>
                        <div class="performance_content_text">
                            「プレジデント（6/14発売）」の特集でにてタダコピのビジネスモデルが紹介されました。<br> <a class="link" target"_blank"="" href="http://www.president.co.jp/pre/backnumber/2010/20100614/">バックナンバーのご紹介</a>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2010.4.16                        </div>
                        <div class="performance_content_text">
                            テレビ東京「たけしのニッポンのミカタ！」にてタダコピのビジネスモデルが紹介されました。<br> <a class="link" target"_blank"="" href="http://www.tv-tokyo.co.jp/mikata/backnumber/100416.html">バックナンバーのご紹介</a>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2010.3.24                        </div>
                        <div class="performance_content_text">
                            「GQ JAPAN（3/24発売）」の特集で<b>「空中店舗からタダコピ、妖怪ふぁんど、 <br>農業ネットワークまで。不況知らずの若き起業家たち。」</b>として<br>タダコピと当社代表取締役菅澤が紹介されました。（P74）<br> <a class="link" target"_blank"="" href="./news100324.html">記事のご紹介</a>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2010.1.19                        </div>
                        <div class="performance_content_text">
                            <a href="./news100119.html">毎日放送&#12288;新番組「ワンクリ」 第一回目放送の「日本の若社長」コーナーにて<br>  当社代表取締役菅澤が紹介されました。</a>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                                                    </div>
                        <div class="performance_content_text">
                             <a href="media.html">2010年〜最新のメディア掲載実績</a>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2009.12.14                        </div>
                        <div class="performance_content_text">
                             「宣伝会議12/15号」にてタダコピの上海進出に関する記事が紹介されました。（P34)                         </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2009.10.29                        </div>
                        <div class="performance_content_text">
                             日本テレビ 『The サンデー NEXT』にて、タダコピ及び、<br> タダコピ新システムが紹介されました。                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2009.10.25                        </div>
                        <div class="performance_content_text">
                             時事通信とCNET Japanで中国でのニュースが紹介されました。<br> <a class="link" target"_new"="" href="http://www.jiji.com/jc/zc?k=200910/2009101900825"> 学生向け無料コピー、上海に進出＝ローソンと提携、来春には北京でも</a><br> <a class="link" target"_new"="" href="http://japan.cnet.com/clip/domestic/story/0,3800097352,20401972,00.htm"> オーシャナイズ、無料コピーサービス「タダコピ」で中国進出</a><br>                         </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2009.10.07                        </div>
                        <div class="performance_content_text">
                            <a target"_new"="" href="http://japan.cnet.com/marketing/story/0,3800080523,20401192,00.htm"> CNET Japanで新タダコピサービスが紹介されました。</a>                         </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2009.09.29                        </div>
                        <div class="performance_content_text">
                             化学工業日報で子宮頸がん啓発活動が紹介されました。                         </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2009.09.25                        </div>
                        <div class="performance_content_text">
                             毎日新聞、産経新聞で堺市長選挙のPRが紹介されました。                         </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2009.09.19                        </div>
                        <div class="performance_content_text">
                             読売新聞、日経新聞で堺市長選挙PRが紹介されました。                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2009.09.01                        </div>
                        <div class="performance_content_text">
                             9月1日から第８回『<a target="_new" href="http://tgks.vsn.jp/"><b>学生起業家選手権</b></a>』の募集がスタートしました！<br>弊社代表 菅澤が審査員として参加します。                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2009.07.21                        </div>
                        <div class="performance_content_text">
                             <a target="_blank" href="release/20090716150153284.pdf"> 第一生命様会員誌「サクセスVショット」2009年8月号で<br>弊社代表 菅澤のインタビューが掲載されました。[PDF]</a>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2009.07.16                        </div>
                        <div class="performance_content_text">
                             タダコピが広島ホームテレビで紹介されました。                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2009.07.15                        </div>
                        <div class="performance_content_text">
                             タダコピが広島テレビで紹介されました。                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2009.06.15                        </div>
                        <div class="performance_content_text">
                             「神戸新聞」でタダコピの兵庫県知事選挙のPR事例が紹介されました。                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2009.06.12                        </div>
                        <div class="performance_content_text">
                             「読売新聞」でタダコピの東京都議会議員選挙のPR事例が紹介されました。                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2009.06.10                        </div>
                        <div class="performance_content_text">
                             「日本経済新聞」でタダコピの記事が掲載されました。<br> <a target="_blank" href="http://www.nikkei.co.jp/news/retto/20090609c3b0904b09.html">[地域経済 関東] オーシャナイズ、無料コピーに「ターゲティング広告」導入</a>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2009.06.01                        </div>
                        <div class="performance_content_text">
                             「<a target="_blank" href="http://www.enibook.com/">有隣堂×エニグモ</a>」のウェブサイトで弊社代表取締役&#12288;菅澤が本を紹介しています。                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2009.05.15                        </div>
                        <div class="performance_content_text">
                             「宣伝会議5/15号」でタダコピの名古屋市長選挙のＰＲ事例が紹介されました。                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2009.04.15                        </div>
                        <div class="performance_content_text">
                             「名古屋テレビ」でタダコピの名古屋市長選挙のＰＲ事例が紹介されました。                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2009.02.25                        </div>
                        <div class="performance_content_text">
                             「buaiso」で弊社取締役菅澤が紹介されました。                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2008.12.11                        </div>
                        <div class="performance_content_text">
                             NHK『ニューステラス関西』                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2008.11.29                         </div>
                        <div class="performance_content_text">
                             『読売新聞朝刊』                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2008.10.19                         </div>
                        <div class="performance_content_text">
                             TBS『アッコにおまかせ！』                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2008.09.03                         </div>
                        <div class="performance_content_text">
                             週刊朝.MOOK『大学の選び方2008』                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2008.08.07                         </div>
                        <div class="performance_content_text">
                             TBS『ニュースバード』                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2008.06.19                         </div>
                        <div class="performance_content_text">
                             アメーバニュースで「タダコピ」が紹介されました<br> <a target="_blank" href="http://news.ameba.jp/domestic/2008/08/16846.html">[アメーバニュース] 無料でコピーできるサービス「タダコピ」が人気</a>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2008.06.17                         </div>
                        <div class="performance_content_text">
                             『週刊SPA!』                         </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2008.05.28                         </div>
                        <div class="performance_content_text">
                             『頭で儲ける時代』に弊社が紹介されました                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2008.05.17                         </div>
                        <div class="performance_content_text">
                             『産経新聞朝刊』                         </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2008.04.24                         </div>
                        <div class="performance_content_text">
                             『GetNavi6.号』                         </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2008.04.17                         </div>
                        <div class="performance_content_text">
                             TOKYO FM「クロノス」に当社代表取締役筒塩が電話出演しました                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2008.04.15                         </div>
                        <div class="performance_content_text">
                             『宣伝会議4/15号』                         </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2008.04.05                         </div>
                        <div class="performance_content_text">
                             「The Waseda Guardian」に当社代表取締役筒塩が紹介されました                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2008.03.28                         </div>
                        <div class="performance_content_text">
                             求人情報サイト「Reasta」に当社代表取締役筒塩が紹介されました                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2008.02.18                         </div>
                        <div class="performance_content_text">
                             『FujiSankei Business i.』                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2008.02.01                         </div>
                        <div class="performance_content_text">
                             『販促会議3.号』                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2008.01.30                         </div>
                        <div class="performance_content_text">
                             FujiSankei Business i.にて、取締役永澤雄が紹介されました                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2008.01.24                         </div>
                        <div class="performance_content_text">
                             埼玉新聞にて、取締役永澤雄が紹介されました                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2008.01.20                         </div>
                        <div class="performance_content_text">
                             山梨..新聞にて、取締役永澤雄が紹介されました                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2008.01.17                         </div>
                        <div class="performance_content_text">
                             名古屋タイムズにて、取締役永澤雄が紹介されました                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2008.01.15                         </div>
                        <div class="performance_content_text">
                             茨城新聞にて、取締役永澤雄が紹介されました                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2008.01.14                         </div>
                        <div class="performance_content_text">
                             佐賀新聞にて、取締役永澤雄が紹介されました                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2008.01.11                         </div>
                        <div class="performance_content_text">
                             中国新聞にて、取締役永澤雄が紹介されました                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2007.12.10                        </div>
                        <div class="performance_content_text">
                             レタスクラブ（12/25号）                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2007.11.20                        </div>
                        <div class="performance_content_text">
                             東京新聞                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2007.11.13                        </div>
                        <div class="performance_content_text">
                             MODERNVENTURE                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2007.11.12                        </div>
                        <div class="performance_content_text">
                             週刊ダイヤモンド                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2007.11.01                        </div>
                        <div class="performance_content_text">
                             ascii.jp                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2007.10.25                        </div>
                        <div class="performance_content_text">
                             経営者会報                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2007.10.23                        </div>
                        <div class="performance_content_text">
                             TBS『はなまるマーケット「とくまる」』                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2007.10.18                        </div>
                        <div class="performance_content_text">
                             朝日新聞大阪版                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2007.09.29                        </div>
                        <div class="performance_content_text">
                             朝日放送『おはよう朝日                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2007.09.21                        </div>
                        <div class="performance_content_text">
                             フィナンシャルジャパン11月号                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2007.09.19                        </div>
                        <div class="performance_content_text">
                             讀賣テレビ『ズームイン・スーパー』                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2007.09.12                        </div>
                        <div class="performance_content_text">
                             CNETJapanで「タダコピ」の関西進出が紹介されました<br> <a target="_blank" href="http://japan.cnet.com/marketing/story/0,3800080523,20356338,00.htm">無料コピーサービス「タダコピ」が関西進出--導入実績は38大学54キャンパスに</a>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2007.09.04                        </div>
                        <div class="performance_content_text">
                             小学館DIME                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2007.08.14                        </div>
                        <div class="performance_content_text">
                             日本テレビ『スッキリ！』                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2007.07.20                        </div>
                        <div class="performance_content_text">
                             テレビ大阪『ニュースＢＩＺ』                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2007.07.19                        </div>
                        <div class="performance_content_text">
                             TBS『２時っチャオ！』                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2007.07.14                        </div>
                        <div class="performance_content_text">
                             BS朝日『賢者の選択』                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2007.07.11                        </div>
                        <div class="performance_content_text">
                             東海テレビ『追跡!ヒットの秘密』                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2007.07.10                        </div>
                        <div class="performance_content_text">
                             毎日新聞（朝刊25面）                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2007.07.05                        </div>
                        <div class="performance_content_text">
                             asahi.comで「タダコピ」の参議院議員選挙キャンペーンが紹介されました<br> <a target="_blank" href="http://www2.asahi.com/senkyo2007/localnews/tokyo/TKY200707050147.html">コピー紙裏に広告、若者に投票をＰＲ&#12288;都選管</a>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2007.07.04                        </div>
                        <div class="performance_content_text">
                             NHK『おはよう日本』                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2007.06.29                        </div>
                        <div class="performance_content_text">
                             日経MJ                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2007.06.13                        </div>
                        <div class="performance_content_text">
                             NHK『ゆうどきネットワーク』                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2007.06.11                        </div>
                        <div class="performance_content_text">
                             CBC『イッポウ』                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2007.06.10                        </div>
                        <div class="performance_content_text">
                             日経MJ                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2007.06.04                        </div>
                        <div class="performance_content_text">
                             朝日新聞（朝刊27面、愛知面）                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2007.06.03                        </div>
                        <div class="performance_content_text">
                             よみうりテレビ『ほんわかテレビ』                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2007.06.02                        </div>
                        <div class="performance_content_text">
                             NHK教育テレビ『一期一会&#12288;キミにききたい！』に、当社メンバーが出演致しました。                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2007.06.01                        </div>
                        <div class="performance_content_text">
                             中京テレビ『ニュースリアルタイム』                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2007.05.31                        </div>
                        <div class="performance_content_text">
                             TBS『ピンポン』                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2007.05.24                        </div>
                        <div class="performance_content_text">
                             SANKEIEXPRESS                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2007.05.21                        </div>
                        <div class="performance_content_text">
                             よみうりテレビ『おはようコールABC』                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2007.05.13                        </div>
                        <div class="performance_content_text">
                             TBS『朝ズバっ』                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2007.05.11                        </div>
                        <div class="performance_content_text">
                             よみうりテレビ『ニューススクランブル』                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2007.05.09                        </div>
                        <div class="performance_content_text">
                             日本テレビ『リアルタイム』                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2007.03.27                        </div>
                        <div class="performance_content_text">
                             関西テレビ『ナンボDEなんぼ』                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2007.02.16                        </div>
                        <div class="performance_content_text">
                             読売新聞夕刊にて、当社代表取締役筒塩が紹介されました。                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2007.01.23                        </div>
                        <div class="performance_content_text">
                             関西テレビ『スーパーニュースアンカー』                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2007.01.17                        </div>
                        <div class="performance_content_text">
                             CBC『イッポウ』                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2006.12.10                        </div>
                        <div class="performance_content_text">
                             『THE21』                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2006.12.07                        </div>
                        <div class="performance_content_text">
                             よみうりテレビ『ミヤネ屋』                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2006.12.06                        </div>
                        <div class="performance_content_text">
                             フジテレビ『スーパーニュース』                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2006.12.05                        </div>
                        <div class="performance_content_text">
                             TBS『ニュースバード』                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2006.11.21                        </div>
                        <div class="performance_content_text">
                             テレビ東京『モーニングサテライト』                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2006.11.09                        </div>
                        <div class="performance_content_text">
                             Gigazineでタダコピがが紹介されました<br> <a target="_blank" href="http://gigazine.net/index.php?/news/comments/20061025_tadacopy/">0円でコピーできる「タダコピ」ができるまで</a>                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2006.10.23                        </div>
                        <div class="performance_content_text">
                             朝日新聞                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2006.10.20                        </div>
                        <div class="performance_content_text">
                             日本テレビ『NNN News リアルタイム』                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2006.07.01                        </div>
                        <div class="performance_content_text">
                             『ダイヤモンドビジョナリー』                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2006.06.01                        </div>
                        <div class="performance_content_text">
                             『宣伝会議』                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2006.05.20                        </div>
                        <div class="performance_content_text">
                             J-WAVEのラジオ『MAKE IT』に、当社代表が出演致しました。                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2006.04.07                        </div>
                        <div class="performance_content_text">
                             東京新聞、北日本新聞、河北新報                        </div>
                    </div>
                </div>
                            <div class="performance_contentsBox">
                    <div class="performance_content">
                        <div class="performance_content_date">
                           2006.03.20                        </div>
                        <div class="performance_content_text">
                             TBS『ドキュメントナウ』に、当社メンバーが出演致しました。                        </div>
                    </div>
                </div>
                    </div>
    </div>
</div>


<?php include_once("footer.php");?>
<?php include_once("analyticstracking.php") ?></body>
</html>
<?php

/**
 * Send mail with PHPMailer
 *
 * @package		Capture
 * @version		1.0
 * @author		thailvn@gmail.com
 * 
 */

include('phpmailer/bootstrap.php');

class Mailer extends PHPMailer {
    
    public function __construct($config = array()) {
       include('config.php');
       $config = $config['phpmailer'];
       
       foreach ($config as $key => $value) {
        $this->{$key} = $value;
    }
}
}

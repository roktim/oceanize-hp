<?php
require("./functions.php");
$url = $_SERVER["REQUEST_URI"];

$referer = $_GET["referer"];

include_once("header.php");
?>
<script type="text/javascript">
    var _retURL = "index.php";

    function DisableButton(b){
		b.disabled = true;
		b.style.opacity = 0.5;
		b.value = '送信中';

		setTimeout(function(){
			b.disabled = false;
			b.style.opacity = 1;
			b.value = "送　信";
		}, 10000);

		b.form.submit();
    };

</script>

<div id="container" class="mainContact" >
    <div id="containerWrap"></div>
    <div class="contactBox" id="contactBox">
        <div class="contact_title">
            <h1>
                CONTACT
            </h1>
        </div>
        <div >
            <form action="contact_send.php" method="post" target="my_contactframe">
                <div class="contact_type">
                    <label class="contact_radio1 contact_radio_active">
                        <input type="radio" name="type" value="広告主" checked required>
                        <p class="contact_radio_text"><span>広告主の方</span> </p>
                    </label>
                    <label class="contact_radio2" >
                        <input type="radio" name="type" value="広告代理店" required>
                        <p class="contact_radio_text"><span> 広告代理店の方</span> </p>
                    </label>
                    <label class="contact_radio3" >
                        <input type="radio" name="type" value="教育機関" required>
                        <p class="contact_radio_text"><span>教育機関の方</span> </p>
                    </label>
                    <label class="contact_radio3" >
                        <input type="radio" name="type" value="学生団体の方" required>
                        <p class="contact_radio_text"><span>学生団体の方</span> </p>
                    </label>
                </div>
                <div class="inputBox">
                    <label class="contact_label" for="name">お名前</label>
                    <input name="name" type="text" value="" id="name" placeholder="お名前を入力" required>
                </div>
                <div class="inputBox">
                    <label class="contact_label mail" for="mail">メールアドレス</label>
                    <input class="mail" name="mail" type="email" value="" id="mail" placeholder="メールアドレスを入力" required>
                </div>
                <div class="inputBox">
                    <label class="contact_label tel" for="tel">電話番号</label>
                    <input name="tel" type="tel" value="" id="tel" placeholder="電話番号を入力" required>
                </div>
                <div class="inputBox textarea">
                    <label for="textContent" class="contact_label">お問合せ内容</label>
                    <textarea name="textContent" cols="50" rows="10" id="textContent" placeholder="お問合せ内容を入力" required></textarea>
                </div>
                <input type="hidden" name="referer" value="<?php echo $referer; ?>" >
                <input type="submit" id="form_send_button" class="form_send" value="送　信" onclick="DisableButton(this);">
            </form>
        </div>
        <div style="clear: both; height: 10px;"></div>
        <iframe name="my_contactframe" id ="my_contactframe" style="display: none;">
        </iframe>
        <div class="popbox">
            <div class="open" id="showPopup"> </div>
            <div class="collapse">
                <div class="box">
                    <img alt="" src="img/magazine_pop.png" class="popbanner">
                    <div class="close" style="margin: 5px">
<!--                        <img alt="" src="img/tadacopyPop_icon.png">-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function thankPop() {
        setTimeout(function () {
            $("#showPopup").click();
        }, 400);
    }
</script>

<?php include_once("footer.php"); ?>
<?php include_once("analyticstracking.php") ?></body>
</html>

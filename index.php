<script type="text/javascript">
  if (screen.width < 640) {
    document.location = "indexSp.php";
  }
</script>



<?php

require("./functions.php");
$url = $_SERVER["REQUEST_URI"];

include_once("header.php");
?>


<div id="container">
  <!-- お問い合わせへのレスポンス -->
  <?php if (isset($_SESSION['success'])): ?>
    <div class="popbox" onClick="sessionSuccess();">
      <div class='collapse'>
        <div class='box sessionSuccess'>
          お問い合わせ、ありがとうございました。
        </div>
      </div>
    </div>
    <?php unset($_SESSION['success']); ?>
  <?php endif; ?>


  <div class="indexBox">

    <div class="top_readImage">
      <img class="top_readImage" src="./img/top_readImage.png" alt="oceanize">
      <a class="top_learn_more" href="./statement.php">
        learn more
        <img src="./img/link_icon.png" alt="oceanize">
      </a>
    </div>

    <div class="index_headline">
      <img class="sp_none" src="img/top_readText.png" alt="私たち、オーシャナイズは学生を動かすことで、世界を動かしていく。">
      <span class="pc_none">
        <h1>
          私たち、オーシャナイズは学生を動かすことで、世界を動かしていく。
        </h1>
      </span>
    </div>
    <div class="index_contentsBoxTop">

      <div class="index_content Num1">
        <div class="popbox">
<!--          <img class="open" src="./img/tadacopy_logo.png" alt="タダコピ">-->
          <img class="open" src="./img/logos/logo-1.png" alt="タダコピ">
          <div class="index_ContentText open">
            Copy for free.<br />
            Be free to enjoy campus life.<br />
            service:tadacopy  since:2005
          </div>
          <div class='collapse'>
            <div class='box'>
                <a target="_blank" href="http://www.tadacopy.com/clients.html#b1">
                    <img class="popbanner" src="img/tadacopyPop.png" alt="">
                    <span class="read_more">  <img src="./img/all_more_link.png" alt=""></span>
                </a>
              <a class="popLogo popup-1" target="_blank" href="http://www.tadacopy.com/clients.html#b1">
<!--                <img src="img/tadacopyPop_logo.png" alt="タダコピ">-->
                <img src="img/popup_logos/TC-18_03.png" alt="タダコピ">
              </a>
              <a class="popText" target="_blank" href="http://www.tadacopy.com/clients.html#b1">
                <img src="img/tadacopyPop_text.png" alt="">
              </a>
              <div class="close">
                <img src="img/tadacopyPop_icon.png" alt="">
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="index_content Num2">
        <div class="popbox">
<!--          <img class="open" src="./img/tadach_logo.png" alt="タダch">-->
          <img class="open" src="./img/logos/logo-2.png" alt="タダch">
          <div class="index_ContentText open">
            Explore your new world.<br />
            Real-time broadcast for Students.<br />
            service:tadacopy channel  since:2014
          </div>
          <div class='collapse'>
            <div class='box'>
                <a target="_blank" href="http://www.tadacopy.com/clients.html#b4">
                    <img class="popbanner" src="img/tadachPop.png" alt="">
                    <span class="read_more">  <img src="./img/all_more_link.png" alt=""></span>
                </a>
              <a class="popLogo popup-2" target="_blank" href="http://www.tadacopy.com/clients.html#b4">
<!--                <img src="img/tadachPop_logo.png" alt="タダch">-->
                <img src="img/popup_logos/popup-logo-2.png" alt="タダch">
              </a>
              <a class="popText" target="_blank" href="http://www.tadacopy.com/clients.html#b4">
                <img src="img/tadachPop_text.png" alt="">
              </a>
              <div class="close">
                <img src="img/tadachPop_icon.png" alt="">
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="index_content Num3">
        <div class="popbox">
<!--          <img class="open" src="./img/tadacopy2_logo.png" alt="タダコピ">-->
          <img class="open" src="./img/logos/logo-3.png" alt="タダコピ">
          <div class="index_ContentText open">
            Fun to use tadacopy.<br />
            It is a students hub.<br />
            service:tadacopy app  since:2014
          </div>
          <div class='collapse'>
            <div class='box'>
                <a  target="_blank" href="http://www.tadacopy.com/app/">
                    <img class="popbanner" src="img/tadacopy2Pop.png" alt="">
                    <span class="read_more">  <img src="./img/all_more_link.png" alt=""></span>
                </a>
              <a class="popLogo popup-3" target="_blank" href="http://www.tadacopy.com/app/">
<!--                <img src="img/tadacopy2Pop_logo.png" alt="タダコピ">-->
                <img src="img/popup_logos/popup-logo-3.png" alt="タダコピ">
              </a>
              <a class="popText" target="_blank" href="http://www.tadacopy.com/app/">
                <img src="img/tadacopy2Pop_text.png" alt="">
              </a>
              <div class="close">
                <img src="img/tadacopy2Pop_icon.png" alt="">
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="index_content Num4">
        <div class="popbox">
          <img class="open" src="./img/unidol_logo.png" alt="UNIDOL">
<!--          <img class="open" src="./img/logos/logo-4.png" alt="UNIDOL">-->
          <div class="index_ContentText open">
            Who is your favorite!?<br />
            Japanese University idols are here.<br />
            service:unidol  since:2014
          </div>
          <div class='collapse'>
            <div class='box'>
                <a target="_blank" href="http://unidol.jp/">
                    <img class="popbanner" src="img/unidolPop.png" alt="">
                    <span class="read_more">  <img src="./img/unidol_read_more.png" alt=""></span>
                </a>
              <a class="popLogo" target="_blank" href="http://unidol.jp/">
                <img src="img/unidolPop_logo.png" alt="UNIDOL">
              </a>
              <a class="popText" target="_blank" href="http://unidol.jp/">
                <img src="img/unidolPop_text.png" alt="">
              </a>
              <div class="close">
                <img src="img/unidolPop_icon.png" alt="">
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="index_content Num5">
        <div class="popbox">
          <img class="open" src="./img/toretan_logo.png" alt="トレタン">
<!--          <img class="open" src="./img/logos/logo-5.png" alt="トレタン">-->
          <div class="index_ContentText open">
            Study Hub for classroom.<br />
            Share tips,save time,geting smart.<br />
            service:toretan!  since:2014
          </div>
          <div class='collapse'>
            <div class='box'>
                <a target="_blank" href="https://toretan.jp/">
                    <img class="popbanner" src="img/toretanPop.png" alt="">
                    <span class="read_more">  <img src="./img/unidol_read_more.png" alt=""></span>
                </a>
              <a class="popLogo" target="_blank" href="https://toretan.jp/" alt="トレタン">
                <img src="img/toretanPop_logo.png" alt="">
              </a>
              <a class="popText" target="_blank" href="https://toretan.jp/">
                <img src="img/toretanPop_text.png" alt="">
              </a>
              <div class="close">
                <img src="img/toretanPop_icon.png" alt="">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="index_contentsBoxBottom">
      <div class="index_content Num6">
        <div class="popbox">
<!--          <img class="open" src="./img/canpass_logo.png" alt="CANPASS">-->
          <img class="open" src="./img/logos/logo-6.png" alt="CANPASS">
          <div class="index_ContentText open">
            Get sweet deals & coupon.<br />
            Campus life is so sweet!<br />
            service:can>pass  since:2014
          </div>
          <div class='collapse'>
            <div class='box'>
<!--              <img class="popbanner" src="img/canpassPop.png" alt="">-->
                <a target="_blank" href="http://canpassapp.com/">
                    <img class="popbanner" src="img/popup_logos/canpass.png" alt="">
                    <span class="read_more">  <img src="./img/canpass_more_link.png" alt=""></span>
                </a>
              <a class="popLogo popup-6" target="_blank" href="http://canpassapp.com/">
<!--                <img src="img/canpassPop_logo.png" alt="CANPASS">-->
                <img src="img/popup_logos/popup-logo-6.png" alt="CANPASS">
              </a>
              <a class="popText" target="_blank" href="http://canpassapp.com/">
<!--                <img src="img/canpassPop_text.png" alt="">-->
                <img src="img/popup_logos/logo-6-txt.png" alt="">
              </a>
              <div class="close" style="width: 15px;">
                <img src="img/canpass_cross.png" alt="">
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="index_content Num7">
        <div class="popbox">
          <img class="open" src="./img/logos/logo-7.png" alt="MAGAZINE">
<!--          <img class="open" src="./img/magazine_logo.png" alt="MAGAZINE">-->
          <div class="index_ContentText open">
            Written by students, for students.<br />
            Community for great campus life.<br />
            service:MAGAZINE  since:2015
          </div>
          <div class='collapse'>
            <div class='box'>
                <a target="_blank" href="http://mag.canpassapp.com/">
                    <img class="popbanner" src="img/magazinePop.png" alt="MAGAZINE">
                    <span class="read_more">  <img src="./img/students_magagin_more_link.png" alt=""></span>
                </a>
              <a class="popLogo popup-7" target="_blank" href="http://mag.canpassapp.com/">
<!--                <img src="img/magazinePop_logo.png" alt="">-->
                <img src="img/popup_logos/magazin_logo.png" alt="">
              </a>
              <a class="popText" target="_blank" href="http://mag.canpassapp.com/">
<!--                <img src="img/magazinePop_text.png" alt="">-->
                <img src="img/popup_logos/magazon_txt.png" alt="">
              </a>
              <div class="close" style="width: 15px;">
                <img src="img/students_magagin_cross.png" alt="">
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="index_content Num8">
        <div class="popbox">
          <img class="open" src="./img/capture_logo.png" alt="Capture">
<!--          <img class="open" src="./img/logos/logo-8.png" alt="Capture">-->
          <div class="index_ContentText open">
            Capture the moment,<br />
            get your dream job.<br />
            service:Capture  since:2015
          </div>
          <div class='collapse'>
            <div class='box'>
                <a target="_blank" href="">
                    <img class="popbanner" src="img/capturePop.png" alt="Cature">
                    <span class="read_more">  <img src="./img/capture_more_link.png" alt=""></span>
                </a>
              <div class="popLogo" target="_blank" href="">
<!--                <img src="img/capturePop_logo_pc.png" alt="" class="capture_pc"> -->
                <img src="img/popup_logos/logo_8_pc.png" alt="" class="capture_pc">
                  <!-- When release app ==> remove class 'capture_pc' and remove _pc in src of image-->
              </div>
              <div class="popText" target="_blank" href="">
                <!-- <a href="http://capture-news.jp/lp"> -->
                  <img src="img/capturePop_text.png" alt="">
               <!--  </a> -->
              </div>
              <div class="close">
                <img src="img/capturePop_icon.png" alt="" >
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="index_content Num9">
        <div class="popbox">
          <img class="open" src="./img/teppan_logo.png" alt="TEPPAN">
<!--          <img class="open" src="./img/logos/logo-9.png" alt="TEPPAN">-->
          <div class="index_ContentText open">
            Simplicity and efficiency<br />
            email app for student job seekers.<br />
            service:TEPPAN  since:2015
          </div>
          <div class='collapse'>
            <div class='box'>
                <a target="_blank" href="http://teppan.email/lp">
                    <img class="popbanner" src="img/teppanPop.png" alt="TEPPAN">
                    <span class="read_more">  <img src="./img/capture_more_link.png" alt=""></span>
                </a>
              <a class="popLogo" target="_blank" href="http://teppan.email/lp">
                <img src="img/teppanPop_logo_pc.png" alt="" class="teppan_pc"><!-- When release app ==> remove class 'teppan_pc' and remove _pc in src of image-->
              </a>
              <a class="popText" target="_blank" href="http://teppan.email/lp">
                  <img src="img/teppanPop_text.png" alt="">
                </a>
              <div class="close">
                <img src="img/teppanPop_icon.png" alt="" >
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="index_content Num10">
        <div class="popbox">
          <img class="open" src="./img/senpai_logo.png" alt="senpai">
<!--          <img class="open" src="./img/logos/logo-10.png" alt="senpai">-->
          <div class="index_ContentText open">
            Ask anything to the college students,<br />
            anytime, anywhere.<br />
            service:SENPAI  since:2015
          </div>
          <div class='collapse'>
            <div class='box'>
                <a target="_blank" href="#" style="display:inline-block; width:100%;height:100%;">
<!--              <img class="popbanner" src="img/senpaiPop.png" alt="senpi">-->
                    <span class="read_more">  <img src="./img/more_link2.png" alt=""></span>
              <div class="popLogo popup-10" target="_blank" href="">
<!--                <img src="img/senpaiPop_logo_pc.png" alt="" class="senpai_pc">-->

                    <img src="img/popup_logos/logo_10_pc.png" alt="" class="senpai_pc">
                     <span class="comming_soon"><img src="img/popup_logos/comming_soon.png" alt=""></span>

                  <!-- When release app ==> remove class 'senpai_pc' and remove _pc in src of image-->
              </div>
              <div class="popText popup-10-txt" target="_blank" href="">
                <img class="name_txt" src="img/senpaiPop_text.png" alt="">

              </div>
                <p><img class="desc_text" src="img/popup_logos/senpai_txt.png" alt=""></p>
                </a>
              <div class="close">
                <img src="img/senpaiPop_icon.png" alt="" >
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="slideBox">
      <div class="ticker" rel="fade">
        <div class="tickerTitle">
          NEWS
        </div>
        <ul class="news_media">
        <li class="tgt"><a href="./performance.php">2017.8.2　株式会社スマートライフとの資本業務提携のお知らせ
          </a></li>
        <li class="tgt"><a href="https://recruit.oceanize.co.jp/" target="_blank">2017.7.4　採用ページをリニューアルいたしました。
          </a></li>
        <li class="tgt"><a href="./performance.php">2017.6.26　中高生向け授業動画サービス「MANAVIE」β版をリリースしました！
          </a></li>
          <li class="tgt"><a href="./performance.php">2017.6.19　SmartSchool麻布本校主催　小学生向け「社会とつながるサマースクール」を実施します！
          </a></li>
          <li class="tgt"><a href="./performance.php">2017.5.1　自社運営教室「SmartSchool 麻布本校」開校！
          </a></li>
        <li class="tgt"><a href="./performance.php">2017.4.13　北九州市立大学・北方キャンパスにタダコピ登場！
          </a></li>
        <li class="tgt"><a href="./performance.php">2017.4.13　北九州市立大学・ひびきのキャンパスにタダコピ登場！
          </a></li>
        <li class="tgt"><a href="./company.php">2017.3.24　オフィスを移転いたしました。
          </a></li>
        <li class="tgt"><a href="./performance.php">2017.3.3　大阪教育大学・柏原キャンパスにタダコピ登場！
          </a></li>
         <li class="tgt"><a href="./performance.php">2016.8.1　写真共有アプリ『PICON』事業譲受のお知らせ </a></li>
        <li class="tgt"><a href="./performance.php">2016.7.19　大東文化大学・東松山キャンパスにタダコピ登場！
          </a></li>
        <li class="tgt"><a href="./performance.php">2016.6.24　岡山理科大学にタダコピ登場！
          </a></li>
        <li class="tgt"><a href="./performance.php">2016.4.22　「大学生のお金の教科書2016 ~大学生の夢と現実とお金の話。」
			10万部を全国の大学（57大学74キャンパス）で配布！</a></li>
          </ul>
        </div><!--/.ticker-->

        <div class="ticker2" rel="fade">
          <div class="tickerTitle">
            MEDIA
          </div>
          <ul class="news_media">
          	<li class="tgt2"><a href="./performance.php">2017.9.6　大学に広がる無料コピー機「タダコピ」の記事が、
          	産経新聞、産経ニュースに掲載されました。</a></li>
            <li class="tgt2"><a href="./performance.php">2015.3.14　女子大生アイドル日本一決定戦「UNIDOL」の記事が、
              朝日新聞、朝日新聞DIGITALに掲載されました。</a></li>
              <li class="tgt2"><a href="./performance.php">2015.3.11　学生の金融リテラシー向上を目指すＳＭＢＣ日興証券様との共同プロジェクト
                「ＵＮＩＳＡ（ユニーサ）」がメディアに掲載されました。</a></li>
                <li class="tgt2"><a href="./performance.php">2014.10.8　四月一日企画・シブヤテレビジョンと共同で行った、
                  LINEクリエイタースタンプの人気投票結果が掲載されました。</a></li>
                  <li class="tgt2"><a href="./performance.php">2014.9.15　日本経済新聞にてタダコピが紹介されました。</a></li>
                  <li class="tgt2"><a href="./performance.php">2014.7.28　「UNIDOL(ユニドル)」が各種ニュースサイトで紹介されました。</a></li>
                  <li class="tgt2"> <a href="./performance.php">2014.6.23　ウェブアンケート作成ツール『SurveyMonkey』（サーベイモンキー）と
                    2014年4月に大学生911人が対象に共同調査したデータが
                    各WEBメディアに掲載されました。</a></li>
                    <li class="tgt2"><a href="./performance.php">2014.5.29　弊社が主催するイベント『UNIDOL 2014 Summer』がyahooニュースをはじめ、
                      各ニュースサイトにて紹介されました。</a></li>
                    </ul>
                  </div><!--/.ticker2-->
                </div>




              </div>
            </div>




            <?php include_once("footer.php");?>
          <?php include_once("analyticstracking.php") ?></body>
          </html>

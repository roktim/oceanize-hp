<?php
require("./functions.php");
$url = $_SERVER["REQUEST_URI"];
include_once("header.php");
?>
<div id="container">
    <div id="containerWrap"></div>
    <div class="companyBox">
        <div class="company_title">
            <span class="sp_none">COMPANY</span>
            <img class="pc_none" src="img/Company_sma01@3x.png" width="80px">
            <!-- <img class="pc" src="img/company-01.png" width="80px"> -->
        </div>
        <div class="company_contents">
<!--            <img class="asiaMapImage pc_none" src="img/Company_sma02@3x.png" width="258px">-->
<!--            <img class="sp_none" src="img/company-02.png" />-->
            <h1 style="display: none;">株式会社オーシャナイズ</h1>
                <div class="company_text_position">
             	    <div class="company_detail">
                            <div class="company_leftContentsBox">
                                <div class="company_Content_title">
                                    【会社名】
                                </div>
                                <div class="company_Content company_name">
                                    株式会社オーシャナイズ
                                </div>
                            </div>
                            <div class="company_leftContentsBox">
                                <div class="company_Content_title">
                                    【英文表記】
                                </div>
                                <div class="company_Content company_eng">
                                    Oceanize,inc.
                                </div>
                            </div>
                            <div class="company_leftContentsBox">
                                <div class="company_Content_title">
                                    【設立】
                                </div>
                                <div class="company_Content company_start">
                                    2005年11月28日
                                </div>
                            </div>
                            <div class="company_leftContentsBox">
                                <div class="company_Content_title">
                                    【資本金】
                                </div>
                                <div class="company_Content company_Content_capital">
                                    <span>1億5</span>,<span>168</span>万円<span>(</span>資本準備金含む<span>)</span>
                                </div>
                            </div>
                            <div class="company_leftContentsBox">
                              <div class="company_Content_title">
                                【従業員数】
                              </div>
                              <div class="company_Content company_Content_intern" style="letter-spacing: 1px;">
                                <!--
                                  <span>30</span>名<span>(</span>インターン<span class="dot">、</span>アルバイト含む<span>)</span>
                                -->
                                <span>200</span>名<span>(</span>子会社含む<span class="dot">)</span>
                              </div>
                            </div>
                            <div class="company_leftContentsBox">
                                <div class="company_Content_title none_float">
                                    【本社所在地】
                                </div>
                                <div class="company_Content_Under">
                                    <span class="company_Content_zipcode">〒106-0031</span>東京都港区西麻布<span class="company_content_address">3-22-10</span><br />
                                </div>
                                <div class="company_Content_Under second_line">
                                    <span>StudentsArk</span>
                                </div>
                                <div class="company_Content_Under second_line">
                                    <span>Tel: 03-6432-9221</span>
                                </div>
                            </div>
                            <div class="company_leftContentsBox">
                                <div class="company_Content_title none_float" style="width:300px;">
                                    【スマートキャンパス事業オフィス】
                                </div>
                                <div class="company_Content_Under">
                                    <span class="company_Content_zipcode">〒169-0074</span>東京都新宿区北新宿<span class="company_content_address">2-21-1</span><br />
                                </div>
                                <div class="company_Content_Under second_line">
                                    <span>新宿フロントタワー 34F イノベーターズ内</span>
                                </div>
                                <div class="company_Content_Under second_line">
                                    <span>Tel: 03-5358-9448</span>
                                </div>
                            </div>
                            <div class="company_leftContentsBox">
                                <div class="company_Content_title none_float" style="width:300px;">
                                    【銀座オフィス】
                                </div>
                                <div class="company_Content_Under">
                                    <span class="company_Content_zipcode">〒104-0061</span>東京都中央区銀座<span class="company_content_address">1-7-10</span><br />
                                </div>
                                <div class="company_Content_Under second_line">
                                    <span>ヒューリック銀座ビル 4F</span>
                                </div>
                                <div class="company_Content_Under second_line">
                                    <span>Tel: 03-3535-0569</span>
                                </div>
                            </div>
                            <div class="company_leftContentsBox">
                                <div class="company_Content_title none_float" style="width:300px;">
                                    【大阪支店】
                                </div>
                                <div class="company_Content_Under">
                                    <span class="company_Content_zipcode">〒550-0013</span>大阪府大阪市西区新町<span class="company_content_address">1-25-6</span><br />
                                </div>
                                <div class="company_Content_Under second_line">
                                    アナスタシア新町一丁目<span>105</span>号室
                                </div>
                            </div>
                            <div class="company_leftContentsBox">
                                <div class="company_Content_title none_float" style="width:300px;">
                                    【福岡支店】
                                </div>
                                <div class="company_Content_Under">
                                    <span class="company_Content_zipcode">〒812-0008</span>福岡県福岡市博多区東光2丁目<span class="company_content_address">13-9</span><br />
                                </div>
                                <div class="company_Content_Under second_line">
                                    ザシダーハウスバイサヴォイ <span>4</span>階
                                </div>
                            </div>
                        </div>

                        <div class="company_detail_under">
                            <div class="company_leftContentsBox">
                                <div class="company_Content_title sub1">
                                    【役員】
                                </div>
                                <div class="company_Content sub1">
                                    代表取締役 菅澤 聡<br />
                                    取締役副社長 筒塩 快斗<br />
                                    取締役 北山 哲朗<br />
                                    取締役CFO 赤間 健太<br />
                                    執行役員 経営企画室長 飯沼 晃史<br />
                                    執行役員 スマートスクール事業部長 段原 亮治<br />
                                    社外取締役 齋藤 太郎<br />
                                    社外取締役 中原 貴裕<br />
                                    監査役 伊庭野 基明
                                </div>
                            </div>
                            <!--
                            <div class="company_leftContentsBox">
                                <div style="height: 16px;"></div>
                                <div class="company_Content_title sub1 bank">
                                    【主要取引銀行】
                                </div>
                                <div class="company_Content sub1">
                                    みずほ銀行京橋支店<br />
                                    りそな銀行新都心営業部<br />
                                    ハ千代銀行渋谷支店<br />
                                    三菱東京UFJ銀行渋谷中央支店<br />
                                    三菱東京UFJ銀行六本木支店
                                </div>
                            </div>
                            -->
                        </div>
                    </div>
        </div>
        <div id="map_canvas" style="width: 957px; height: 350px;"></div>
        <!-- <img src="./img/map.png"> -->
        <div class="asiaMap">
<!--            <img class="asiaMapImage" src="img/company-04.png" >-->
<!--            <img class="asiaMapImage pc_none" src="img/Company_sma04@3x.png" style= "width:272px; margin: 2px 0 0 1px;">-->
           <p class="">
                【海外拠点】
            </p>
            <!--
            <div class="asiaArea_left">
                <div class="square"></div><span>オーシャナイズ</span>  China  :<br />
                                Room 304, Huanzhong Building,<br />
                                175 South Xiangyang Road,<br />
                                Xuhui District, Shanghai, China, 200031

            </div>
            -->
            <div class="asiaArea_left">
                <div class="square"></div><span>オーシャナイズ</span>  Vietnam :<br />
                                7F TS Building No.17 Street No 2,<br />
                                Cu Xa Do Thanh, Ward 4,<br />
                                District 3, Ho Chi Minh City  <span>従業員</span> 10名
<!--                <img  src="img/company-05.png" >-->
            </div>
            <div class="asiaArea_middle">
                <div class="square"></div><span>オーシャナイズ</span>  Bangladesh :<br />
                Flat A3, House 257, Road 19/A, <br />
                New DOHS,  Mohakhali, <br/>Dhaka-1216, Bangladesh <span>従業員</span> 10名
            </div>
            <div class="asiaArea_right">
                <div class="square"></div><span>オーシャナイズ</span>  台湾 :<br />
                台北市大安區忠孝東路四段270號17F <br />
                (美孚時代通商大樓) <span>従業員</span> 10名
            </div>
            <div id="map_canvas2" style="width: 957px; height: 350px;"></div>
            <!-- <img src="./img/asiamap.png"> -->
        </div>
        <!--New logo-->
        <div class="newLogoCon">
            <p>【子会社】</p>
            <div class="newLogoCon_left">
            	<a href="http://seazoo.co.jp/" target="_blank" >
                <div class="square"></div><span>株式会社シーズー</span> :<br />
                学生領域で培ったR&Dの経験と<br />
                ノウハウを他社との事業に活用し、<br />
                メディア(オンライン&オフライン)の開発、<br/>
                保守運用、販売等をワンストップでご提供。<br/>
                <div class="seezooLogo">
                    <img src="img/seezoo.png" alt="SeeZoo Logo">
                </div>
                <!-- /.seezooLogo -->
            </div>
            <div class="newLogoCon_middle">
            	<a href="http://oceanhawks.co.jp" target="_blank" >
                <div class="square"></div><span>株式会社オーシャンホークス</span> :<br />
                優秀な開発エンジニアが豊富な<br />
                バングラデシュを中心とした<br />
                東アジア諸国でのオフショア開発及び <br/>
                ラボ型開発チーム組成のご提案。<br/>
                <div class="ocHwLogo">
                    <img src="img/ocanhawks.png" alt="Oceanize Hawks">
                </div>
                </a>
                <!-- /.ocHwLofo -->
            </div>
            <div class="newLogoCon_right">
                <div class="square"></div><span>株式会社オーシャナイズキャリア</span> :<br />
                大学生のキャリア教育及び、<br />
                大学生の短期派遣・新卒紹介。<br />
                大学生ネットワークのリソースを活用した <br/>
                アウトソーシングのご提案。<br/>
                <div class="poeredByOC">
                    <img src="img/poweredbyoc.png" alt="Powered by Oceanize">
                </div>
                <!-- /.poeredByOC -->
            </div>
        </div>
        <!-- /.newlogo -->

    </div>
</div>




<?php include_once("footer.php"); ?>
<?php include_once("analyticstracking.php") ?></body>
</html>

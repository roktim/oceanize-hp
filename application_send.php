<?php

session_start();

$type = $_POST["type"];
$work = $_POST["work"];
$name = $_POST["name"];
$mail = $_POST["mail"];
$tel = $_POST["tel"];
$textContent = $_POST["textContent"];

//==========================================================================
//メールmsg
$msg = "{$name}様から";
$msg.="エントリーメールが届きました。\n";


//メールBODY
$body = <<<BODY
===  エントリー内容  ===
【氏名】  {$name} \n\r
【電話番号】 {$tel} \n\r
【メールアドレス】 {$mail} \n\r
【種別】 {$type} \n\r
【業務区分】{$work} \n\r
【志望理由】 \n\r
{$textContent} \n\r
 \n\r

BODY;



//メールfotter
$footer = "━━━━━━━━━━━━━━━━━━━━━━━━━━━\n";
$footer .= "株式会社オーシャナイズ\n\n";
$footer .= "〒106-0031\n";
$footer .= "東京都港区西麻布3-22-10 StudentsArk\n";
$footer .= "t 03-6432-9221\n";
$footer .= "f 03-6447-4594\n";
$footer .= "【コーポレートサイト】    ：http://oceanize.co.jp\n";
$footer .= "【タダコピ】              ：http://www.tadacopy.com/\n";
$footer .= "━━━━━━━━━━━━━━━━━━━━━━━━━━━\n";


//自動返信用
//$mail_to = $m_mail;
$subject    = "【エントリー確認メール】（自動返信）";
$header     = "From:recruit@oceanize.co.jp";

mb_language("Japanese");
$oc_to      = "webmaster@oceanize.co.jp";
$oc_subject = "【entry】" . $name . "様";
$oc_header  = $header ;
//OC向けに送信
mb_language("Japanese");
$body    	= str_replace("\n", "", $body);
$oc_body  	= $msg . $body . $footer; //bodyの組み立て
//メール送信終わり
/*
 * NEW SEND MAIL
 */

include('mailer.php');

$mailler            = new Mailer();
$mailler->From      = "irecruit@oceanize.co.jp";
$mailler->FromName  = $oc_header;
$mailler->AddAddress($oc_to);
$mailler->AddReplyTo($mail);

$mailler->IsHTML(false);                                  // set email format to HTML

$mailler->Subject   = $oc_subject; //"Here is the subject";
$mailler->Body      = $oc_body; //"This is the HTML message body <b>in bold!</b>";

if (!$mailler->Send()) {
    echo "<script type= 'text/javascript'>  alert(\"Message could not be sent. \");</script>";
    exit();
}


//自動返信メールmsg
$msg = "{$name}様\n\n";
$msg.="エントリーを受け付けました。\n";
$msg .="後日、担当の者からご連絡させていただきます。";

$msg.="\n\n今しばらくお待ちくださいませ。";

$msg.="\n\n※このメールは自動で送信しております。\n\n";


//メールBODY
$body = <<<BODY
===  エントリー内容  ===
【氏名】  {$name} \n\r
【電話番号】 {$tel} \n\r
【メールアドレス】 {$mail} \n\r
【種別】 {$type} \n\r
【業務区分】{$work} \n\r
【志望理由】 \n\r
{$textContent} \n\r
 \n\r

BODY;



//メールfotter
$footer = "━━━━━━━━━━━━━━━━━━━━━━━━━━━\n";
$footer .= "株式会社オーシャナイズ\n\n";
$footer .= "〒106-0031\n";
$footer .= "東京都港区西麻布3-22-10 StudentsArk\n";
$footer .= "t 03-6432-9221\n";
$footer .= "f 03-6447-4594\n";
$footer .= "【コーポレートサイト】    ：http://oceanize.co.jp\n";
$footer .= "【タダコピ】              ：http://www.tadacopy.com/\n";
$footer .= "━━━━━━━━━━━━━━━━━━━━━━━━━━━\n";


//自動返信用
//$mail_to = $m_mail;
$subject    = "【エントリー確認メール】（自動返信）";
$header     = "From:recruit@oceanize.co.jp";

mb_language("Japanese");
$oc_to      = $mail;
$oc_subject = "【entry】" . $name . "様";
$oc_header  = $header ;
//OC向けに送信
mb_language("Japanese");
$oc_body  	= $msg . $body . $footer; //bodyの組み立て
//メール送信終わり
/*
 * NEW SEND MAIL
*/

$mailler            = new Mailer();
$mailler->From      = "irecruit@oceanize.co.jp";
$mailler->FromName  = $oc_header;
$mailler->AddAddress($oc_to);
$mailler->AddReplyTo("irecruit@oceanize.co.jp");

$mailler->IsHTML(false);                                  // set email format to HTML

$mailler->Subject   = $oc_subject; //"Here is the subject";
$mailler->Body      = $oc_body; //"This is the HTML message body <b>in bold!</b>";

if (!$mailler->Send()) {
	echo "<script type= 'text/javascript'>  alert(\"Message could not be sent. \");</script>";
	exit();
}


//logの生成------------------------------------------
$file   = "/var/www/logs/entry0901.csv";
$fp     = fopen($file, "a");

$log_time = date("Y/m/d H:i:s");
flock($fp, LOCK_EX);
fwrite($fp, $log_time);
fwrite($fp, ",");
fwrite($fp, $name);
fwrite($fp, ",");
fwrite($fp, $tel);
fwrite($fp, ",");
fwrite($fp, $mail);
fwrite($fp, ",");
fwrite($fp, $type);
fwrite($fp, ",");
fwrite($fp, $work);
fwrite($fp, ",");
fwrite($fp, str_replace("\r\n", "", $textContent));
fwrite($fp, "\n");
flock($fp, LOCK_UN);
fclose($fp);

session_unset();
//$_SESSION['success'] = "お問い合わせ、ありがとうございました。";
//header("Location:" . "index.php");
//header("Location:http://www.oceanize.co.jp/");
echo "<script type= 'text/javascript'> parent.thankPop(); </script>";
exit();
?>
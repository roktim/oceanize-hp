<?php
require("./functions.php");
$url = $_SERVER["REQUEST_URI"];

include_once("header.php");
?>
<script type="text/javascript">
    var _retURL = "index.php";
</script>

<div id="container">
    <div id="containerWrap"></div>
    <div class="applicationBox">
        <div class="application_title">
            <h1>
                APPLICATION FORM
            </h1>
        </div>
        <form action="application_send.php" method="post" target="my_contactframe">
            <div class="application_type">
                <label >
                    <input type="radio" name="type" value="正社員" checked required>
                    <span class="label">正社員</span>
                </label>
                <label >
                    <input type="radio" name="type" value="インターンシップ" required>
                    <span class="label">インターンシップ</span>
                </label>
            </div>
            <div class="application_work">
                <label >
                    <input type="radio" name="work" value="営業" checked required>
                    <span class="label">営業</span>
                </label>
                <label >
                    <input type="radio" name="work" value="企画" required>
                    <span class="label">企画</span>
                </label>
                <label >
                    <input type="radio" name="work" value="エンジニア" required>
                    <span class="label">エンジニア</span>
                </label>
                <label >
                    <input type="radio" name="work" value="デザイナー" required>
                    <span class="label">デザイナー</span>
                </label>
                <div style="clear:both"> </div>
                <!-- wantedlybanner -->
<!--                <div class="application_banner">-->
<!--                    <div class="application_moreInfomation">-->
<!--                        more infomation on-->
<!--                    </div>-->
<!--                    <a href="https://www.wantedly.com/projects/14076" target="_blank">-->
<!--                        <img class="pc" src="img/wantedly_banner.png">-->
<!--                        <img class="mobile" src="img/wantedly_banner.png" width="118px">-->
<!--                    </a>-->
<!--                </div> -->
            </div>

            <div class="inputBox">
                <label class="application_label" for="name">お名前</label>
                <input name="name" type="text" value="" id="name" placeholder="お名前を入力" required>
            </div>
            <div class="inputBox0">
                <label class="application_label" for="mail">メールアドレス</label>
                <input name="mail" type="email" value="" id="mail" placeholder="メールアドレスを入力" required>
            </div>
            <div class="inputBox1">
                <label class="application_label" for="tel">電話番号</label>
                <input name="tel" type="tel" value="" id="tel" placeholder="電話番号を入力" required>
            </div>
            <!-- UPLOAD -->
            <div class="inputBox2 upload_file">
                <label class="application_label" for="cv">履歴書</label>
                <span class="label1">履歴書をアップロードしてください</span>
                <div class="application_file1">
                    UPLOAD FILE
                    <input name="cv" type="file" value="" id="cv">
                </div>
            </div>
            <div class="inputBox3 upload_file">
                <label class="application_label" for="cv">職務経歴書</label>
                <span class="label2">職務経歴書をアップロードしてください</span>
                <div class="application_file2">
                    UPLOAD FILE
                    <input name="resume" type="file" value="" id="resume">
                </div>
            </div> 
            <div class="inputBox4">
                <label for="textContent" class="application_label">志望動機</label>
                <textarea name="textContent" cols="50" rows="10" id="textContent" placeholder="志望動機を入力" required></textarea>
            </div>
            <div class="application_notes">
                ＊webからご応募いただいた後、通常3~4日以内に<br class="pc_none">書類選考の結果・次の選考過程についてご連絡させていただきます。
            </div>
            <input type="submit" class="link_black" value="応　募">
        </form>
    </div>
    <iframe name="my_contactframe" id ="my_contactframe" style="display: none;">
    </iframe>
    <div class="popbox">
        <div class="open" id="showPopup"> </div>
        <div class="collapse">
            <div class="box">
                <img alt="" src="img/magazine_pop.png" class="popbanner">
                <div class="close" style="margin: 5px">
<!--                    <img alt="" src="img/tadacopyPop_icon.png">-->
                </div>
            </div>
        </div>
    </div>

</div>



<script type="text/javascript">
    function thankPop() {
        setTimeout(function () {
            $("#showPopup").click();
        }, 400);
    }
</script>


<?php include_once("footer.php"); ?>

<?php include_once("analyticstracking.php") ?></body>
</html>
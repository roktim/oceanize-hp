(function () {

    var isMobile = {
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function() {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };
    
    $.fn.popbox = function (options) {
        var settings = $.extend({
            selector: this.selector,
            open: '.open',
            box: '.box',
            arrow: '.arrow',
            arrow_border: '.arrow-border',
            close: '.close'
        }, options);

        var methods = {
            open: function (event) {
                event.preventDefault();

                var pop = $(this);
                var box = $(this).parent().find(settings['box']);

                box.find(settings['arrow']).css({'left': box.width() / 2 - 10});
                box.find(settings['arrow_border']).css({'left': box.width() / 2 - 10});

                if (box.css('display') == 'block') {
                    // methods.close();
                } else {
                    $('.popWrap').fadeIn();
                    box.css({'display': 'block'});
                    box.animate({
                        'top': '55%',
                        'margin-top': isMobile.any() ? '-160px' : '-211px'
                    }, {
                        'duration': 1000,
                        'easing': 'easeOutQuint'
                    });
                }
            },
            close: function () {
                if (!$('div.box').is(':visible'))
                    return false;
                $(settings['box']).animate({'top': '0%', 'margin-top': '-422px'}, {
                    duration: 500,
                    easing: 'easeOutQuad',
                    complete: function () {
                        $(this).css({'display': 'none'});
                        $('.popWrap').fadeOut();
                    }
                });
                if ($('#contactBox').length > 0)
                    window.location.href = 'index.php';
            }
        }

        $(document).bind('keyup', function (event) {
            if (event.keyCode == 27) {
                methods.close();
            }
        });

        $(document).bind('click', function (event) {
            if (!$(event.target).closest(settings['selector']).length) {
                methods.close();
            }
        });

        return this.each(function () {
            // $(this).css({'width': $(settings['box']).width()}); // Width needs to be set otherwise popbox will not move when window resized.
            $(settings['open'], this).bind('click', methods.open);
            $(settings['open'], this).parent().find(settings['close']).bind('click', function (event) {
                event.preventDefault();
                methods.close();
            });
        });
    }

}).call(this);

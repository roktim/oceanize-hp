/*******************************************************/
// (function($){
// 	if (window.innerWidth < 640) {
//  var portraitWidth,landscapeWidth;
//  $(window).bind("resize", function(){
//    if(Math.abs(window.orientation) === 0){
//      if(/Android/.test(window.navigator.userAgent)){
//        if(!portraitWidth)portraitWidth=$(window).width();
//      }else{
//        portraitWidth=$(window).width();
//      }
//      $("html").css("zoom" , portraitWidth/375 );
//    }else{
//      if(/Android/.test(window.navigator.userAgent)){
//        if(!landscapeWidth)landscapeWidth=$(window).width();
//      }else{
//        landscapeWidth=$(window).width();
//      }
//      $("html").css("zoom" , landscapeWidth/375 );
//    }
//  }).trigger("resize");
//}
//})(jQuery);




/*******************************************************/
// delay_image
$(window).load(function() {
    //$('#delay_image00').hide();
    $('#delay_image00').delay(0).fadeIn(2000);
	// $('#delay_image01, #delay_image02, #delay_image03, #delay_image04, #delay_image05, #delay_image06, #delay_image07, #delay_image08').hide();
	// $('#delay_image01').delay(0).fadeIn(2000);
	// $('#delay_image02').delay(1000).fadeIn(2000);
	// $('#delay_image03').delay(2000).fadeIn(2000);
	// $('#delay_image04').delay(2500).fadeIn(2000);
	// $('#delay_image05').delay(3500).fadeIn(2000);
	// $('#delay_image06').delay(4500).fadeIn(2000);
	// $('#delay_image07').delay(5500).fadeIn(2000);
	// $('#delay_image08').delay(6500).fadeIn(2000);

    setTimeout(function(){
        $(".page_loader").fadeOut();
    },2000);

});


/*******************************************************/
// sessionSuccessを閉じる
function sessionSuccess() {
    $('.box.sessionSuccess').fadeOut('fast');
}


/*******************************************************/
// スクロールTOP
$(function(){
  $('a[href^=#]').click(function(){
    var speed = 500;
    var href= $(this).attr("href");
    var target = $(href == "#" || href == "" ? 'html' : href);
    var position = target.offset().top;
    $("html, body").animate({scrollTop:position}, speed, "swing");
    return false;
  });
});



/*******************************************************/
// fadeInのタイミングを指定
$(function() {
    var topBtn = $('.homeButton');
    topBtn.hide();
    //スクロールがscllollTopに達したらボタン表示
    $(window).scroll(function () {
		if (screen.width >= 640) {
			return false;
		}
        if ($(this).scrollTop() > 100) {

            $(window).on("scroll touchmove", function(){ //スクロール中に判断する
                $(topBtn).stop(); //アニメーションしている場合、アニメーションを強制停止
                $(topBtn).css('display', 'none').delay(450).fadeIn('slow');
                //スクロール中は非表示にして、450ミリ秒遅らせて再び表示
            });

        } else {
            topBtn.fadeOut();
        }
});
});


/*******************************************************/
// メニューボタン
/*
jQuery( function() {
    jQuery( "#menuSelect" )
        .button( {
            text: false,
            icons: {
                primary: "ui-icon-triangle-1-s"
            }
        } )
        .click( function() {
                var menu = jQuery( this ).parent().next().toggle("slide", {easing: "easeInOutCubic"}, 200).position( {
                    my: "left bottom",
                    at: "left bottom",
                    of: $(".headerContents")
                } );
 				// trans状態に合わせて変形
                if ( $("#p01").css('top') == "0px" ) {
                	transOn();
                    $("#containerWrap").css("display", "block");
                    $("#footerWrap").css("display", "block");
              	} else {
              		transOff();
                    $("#containerWrap").css("display", "none");
                    $("#footerWrap").css("display", "none");
              	}

                jQuery( document ).one( "click", function() {
                    menu.hide("slide", { easing: "easeInOutCubic"}, 200);
                    transOff();
                    $("#containerWrap").css("display", "none");
                    $("#footerWrap").css("display", "none");
                } );
                return false;
        } )
        .parent()
        .buttonset()
        .next()
        .hide()
        .menu();
} );
*/
// jQuery( function() {
//     jQuery( "#menuSelect" )
//         .button( {
//             text: false,
//             icons: {
//                 primary: "ui-icon-triangle-1-s"
//             }
//         } )
//         .click( function() {
//                 var menu = jQuery( this ).parent().next().toggle("blind", {easing: "easeInOutCubic"}, 20000).position( {
//                     my: "left bottom",
//                     at: "left bottom",
//                     of: $(".headerContents")
//                 } );
//                                 // trans状態に合わせて変形
//                 if ( $("#p01").css('top') == "0px" ) {
//                     transOn();
//                 } else {
//                     transOff();
//                 }

//                 jQuery( document ).one( "click", function() {
//                     menu.hide("blind", { easing: "swing"}, 2000);
//                     transOff();
//                 } );
//                 return false;
//         } )
//         .parent()
//         .buttonset()
//         .next()
//         .hide()
//         .menu();
// } );



/*******************************************************/
// メニューボタン
function transOn() {
    $("#p01").animate({
        "top": 8,
        "left": 2,
        "rotate": "-45deg",
        "scale": "1.0, 1.0"
      }, 200, 'swing');

    $("#p02").animate({
        "scale": "0, 1.0"
      }, 200, 'swing');

    $("#p03").animate({
        "bottom": 13,
        "left": 2,
        "rotate": "45deg",
        "scale": "1.0, 1.0"
      }, 200, 'swing');
}

function transOff() {
    $("#p01").animate({
        "top": 0,
        "left": 0,
        "rotate": "0deg",
        "scale": "1, 1"
      }, 200, 'swing');

    $("#p02").animate({
        "scale": "1, 1.0"
      }, 200, 'swing');

    $("#p03").animate({
        "bottom": 0,
        "left": 0,
        "rotate": "0deg",
        "scale": "1, 1.0"
      }, 200, 'swing');
}



/*******************************************************/
// オリジナルラジオ
 $(function(){
        if (screen.width >= 640) {
             $('input:radio').screwDefaultButtons({
                    image: 'url("img/radio_icon.png")',
                    width: 31,
                    height: 30
            });
        } else {
            $('input:radio').screwDefaultButtons({
                    image: 'url("img/radio_icon_sp_18.png")',
                    width: 18,
                    height: 17.5
            });
        }
    });




/*******************************************************/
// カスタムgoogleMap
//詳細版
function initialize() {
	var path = $(location).attr('pathname');

	idx = path.indexOf("company.php");

	if(idx != -1){
		var myOptions = {
			zoom: 16,
			center: new google.maps.LatLng(35.659336, 139.724675),
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};

		/*----- スタイルを定義 -----*/
		var styles = [
		    {stylers:[{hue:"#0055be"},{saturation:89}]},{featureType:"water",stylers:[{color:"#0055be"}]},{featureType:"administrative.country",elementType:"labels",stylers:[{visibility:"off"}]}
		    //{"featureType":"water","stylers":[{"color":"#0055be"},{"visibility":"on"}]},{"featureType":"landscape","stylers":[{"color":"#f2f2f2"}]},{"featureType":"road","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"on"}]},{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"transit","stylers":[{"visibility":"on"}]},{"featureType":"poi","stylers":[{"visibility":"on"}]}    ];
		]
		/*----- スタイル名の指定 -----*/
		var styleName = 'MyStyle';

		var map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);

		/*----- スタイルの適用 -----*/
		map.mapTypes.set(styleName, new google.maps.StyledMapType(styles, { name: styleName }));
		map.setMapTypeId(styleName);

		var markers = [
		    ['株式会社オーシャナイズ',35.659336, 139.724675],
		    ['スマートキャンパス事業オフィス',35.696303, 139.689492],
		    ['銀座オフィス',35.674255, 139.767724]
		    ];
		for (var i = 0; i < markers.length; i++) {
			var name = markers[i][0];
			var latlng = new google.maps.LatLng(markers[i][1],markers[i][2]);
			createMarker(latlng,name,map)
		}
	}
}

function createMarker(latlng,name,map)
	{
	var infoWindow = new google.maps.InfoWindow();
	var marker = new google.maps.Marker({position: latlng,map: map});
	google.maps.event.addListener(marker, 'click', function() {
		infoWindow.setContent(name);
		infoWindow.open(map,marker);
	});
}
google.maps.event.addDomListener(window, 'load', initialize);




// world版
function initialize2() {
	var path = $(location).attr('pathname');

	idx = path.indexOf("company.php");

	if(idx != -1){

		var myOptions = {
			zoom: 3,
			center: new google.maps.LatLng(26.115986,125.3125),
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};

		/*----- スタイルを定義 -----*/
		var styles = [
		    {stylers:[{hue:"#0055be"},{saturation:89}]},{featureType:"water",stylers:[{color:"#0055be"}]},{featureType:"administrative.country",elementType:"labels",stylers:[{visibility:"off"}]}
		    //{"featureType":"water","stylers":[{"color":"#0055be"},{"visibility":"on"}]},{"featureType":"landscape","stylers":[{"color":"#f2f2f2"}]},{"featureType":"road","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"on"}]},{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"transit","stylers":[{"visibility":"on"}]},{"featureType":"poi","stylers":[{"visibility":"on"}]}    ];
		]
		/*----- スタイル名の指定 -----*/
		var styleName = 'MyStyle';

		var map = new google.maps.Map(document.getElementById("map_canvas2"),myOptions);

		/*----- スタイルの適用 -----*/
		map.mapTypes.set(styleName, new google.maps.StyledMapType(styles, { name: styleName }));
		map.setMapTypeId(styleName);

		var markers = [
		    // googlmapで詳細座標を確認
		        ['株式会社オーシャナイズ',35.659336, 139.724675],
		        ['オーシャナイズ Vietnam',10.772835,106.681283],
		        ['オーシャナイズ Bangladesh',23.835098,90.3670817],
		        ['オーシャナイズ 台湾',25.041201, 121.554778]
		    ];
		for (var i = 0; i < markers.length; i++) {
		var name = markers[i][0];
		var latlng = new google.maps.LatLng(markers[i][1],markers[i][2]);
		createMarker(latlng,name,map)
		}
	}
}

function createMarker(latlng,name,map)
{
var infoWindow = new google.maps.InfoWindow();
var marker = new google.maps.Marker({position: latlng,map: map});
google.maps.event.addListener(marker, 'click', function() {
infoWindow.setContent(name);
infoWindow.open(map,marker);
});
}
google.maps.event.addDomListener(window, 'load', initialize);




/*******************************************************/
// タイピング風一文字づつ表示
function typeOn(index) {
    // index = index % $(".tgt").length;
    var target = $(".tgt")[index];
    var target_a = $(".tgt a")[index];

    // ここで文字を<span></span>で囲む
    $(target).children().andSelf().contents().each(function() {
      if (this.nodeType == 3) {
          $(this).replaceWith($(this).text().replace(/(\S)/g, '<span>$1</span>'));
      }
      });
    $(target_a).children('span').css({'opacity': 0});
    $(target).css({'opacity':1});
    for (var i = 0; i <= $(target_a).children().size(); i++) {
        $(target_a).children('span:eq('+i+')').delay(50*i).animate({'opacity':1},50);
    };
};


function typeOn2(index) {
    // index2 = index2 % $(".tgt2").length;
    var target2 = $(".tgt2")[index];
    var target_a2 = $(".tgt2 a")[index];

    // ここで文字を<span></span>で囲む
    $(target2).children().andSelf().contents().each(function() {
      if (this.nodeType == 3) {
          $(this).replaceWith($(this).text().replace(/(\S)/g, '<span>$1</span>'));
      }
      });

    $(target_a2).children('span').css({'opacity': 0});
    $(target2).css({'opacity':1});
    for (var i = 0; i <= $(target_a2).children().size(); i++) {
        $(target_a2).children('span:eq('+i+')').delay(50*i).animate({'opacity':1},50);
    };
};


// タイピング風一文字づつ表示
// function typeOn() {
//     // ここで文字を<span></span>で囲む
//     $('.showlist, .showlist2').children().andSelf().contents().each(function() {
//       if (this.nodeType == 3) {
//           $(this).replaceWith($(this).text().replace(/(\S)/g, '<span>$1</span>'));
//       }
//       });
//     $('.showlist a').children('span').css({'opacity': 0});
//     $('.showlist').css({'opacity':1});
//     for (var i = 0; i <= $('.showlist a').children().size(); i++) {
//         $('.showlist a').children('span:eq('+i+')').delay(50*i).animate({'opacity':1},50);
//     };

//     // media
//     $('.showlist2 a').children('span').css({'opacity': 0});
//     $('.showlist2').css({'opacity':1});
//     for (var i = 0; i <= $('.showlist2 a').children().size(); i++) {
//         $('.showlist2 a').children('span:eq('+i+')').delay(50*i).animate({'opacity':1},50);
//     };
// };



/*******************************************************/
//newsスライド
$(function(){
  function fade(index) {
    index = index % $(".tgt").length;
    var target = $(".tgt")[index];

    $(target).css({'display': 'block','opacity':0});
    typeOn(index);
    $(target).delay(10000) // 滞在時間1秒
    .fadeOut({
      complete: function () {
        fade(index + 1);
      }
    });

  };

  fade(0);
});

/*******************************************************/
//mediaスライド
$(function(){
  function fade(index) {
    index = index % $(".tgt2").length;
    var target2 = $(".tgt2")[index];

    $(target2).css({'display': 'block','opacity':0});
    typeOn2(index);
    $(target2).delay(10000) // 滞在時間1秒
    .fadeOut({
      complete: function () {
        fade(index + 1);
      }
    });

  };

  fade(0);
});



/*******************************************************/
//newsスライド
// $(function(){
//   $(window).load(function(){
//     var $setElm = $('.ticker');
//     var effectSpeed = 0;
//     var switchDelay = 4000;
//     var easing = 'easeInExpo';

//     $setElm.each(function(){
//       var effectFilter = $(this).attr('rel'); // 'fade' or 'roll' or 'slide'

//       var $targetObj = $(this);
//       var $targetUl = $targetObj.children('ul');
//       var $targetLi = $targetObj.find('li');
//       var $setList = $targetObj.find('li:first');

//       var ulWidth = $targetUl.width();
//       var listHeight = $targetLi.height();
//       $targetObj.css({height:(listHeight)});
//       $targetLi.css({top:'0',left:'0',position:'absolute'});

//       var liCont = $targetLi.length;

//       if(effectFilter == 'fade') {
//         $setList.css({display:'block',opacity:'0',zIndex:'98'}).stop().css({'opacity':0}).addClass('showlist');
//         typeOn();

//         if(liCont > 1) {
//           setInterval(function(){
//             var $activeShow = $targetObj.find('.showlist');
//             // $activeShow.animate({opacity:'0'},effectSpeed,easing,function(){
//             $activeShow.css({opacity:'0'},function(){
//               $(this).next().css({display:'block',opacity:'0',zIndex:'99'}).css({'opacity':0}).addClass('showlist');
//               typeOn();
//               $(this).appendTo($targetUl).css({display:'none',zIndex:'98'}).removeClass('showlist');
//             });
//           },switchDelay);
//         }


//       } else if(effectFilter == 'roll') {
//         $setList.css({top:'3em',display:'block',opacity:'0',zIndex:'98'}).stop().animate({top:'0',opacity:'1'},effectSpeed,easing).addClass('showlist');
//         if(liCont > 1) {
//           setInterval(function(){
//             var $activeShow = $targetObj.find('.showlist');
//             $activeShow.animate({top:'-3em',opacity:'0'},effectSpeed,easing).next().css({top:'3em',display:'block',opacity:'0',zIndex:'99'}).animate({top:'0',opacity:'1'},effectSpeed,easing).addClass('showlist').end().appendTo($targetUl).css({zIndex:'98'}).removeClass('showlist');
//           },switchDelay);
//         }
//       } else if(effectFilter == 'slide') {
//         $setList.css({left:(ulWidth),display:'block',opacity:'0',zIndex:'98'}).stop().animate({left:'0',opacity:'1'},effectSpeed,easing).addClass('showlist');
//         if(liCont > 1) {
//           setInterval(function(){
//             var $activeShow = $targetObj.find('.showlist');
//             $activeShow.animate({left:(-(ulWidth)),opacity:'0'},effectSpeed,easing).next().css({left:(ulWidth),display:'block',opacity:'0',zIndex:'99'}).animate({left:'0',opacity:'1'},effectSpeed,easing).addClass('showlist').end().appendTo($targetUl).css({zIndex:'98'}).removeClass('showlist');
//           },switchDelay);
//         }
//       }
//     });
//   });
// });




/*******************************************************/
//mediaスライド
// $(function(){
//   $(window).load(function(){
//     var $setElm = $('.ticker2');
//     var effectSpeed = 0;
//     var switchDelay = 4000;
//     var easing = 'easeInExpo';

//     $setElm.each(function(){
//       var effectFilter = $(this).attr('rel'); // 'fade' or 'roll' or 'slide'

//       var $targetObj = $(this);
//       var $targetUl = $targetObj.children('ul');
//       var $targetLi = $targetObj.find('li');
//       var $setList = $targetObj.find('li:first');

//       var ulWidth = $targetUl.width();
//       var listHeight = $targetLi.height();
//       $targetObj.css({height:(listHeight)});
//       $targetLi.css({top:'0',left:'0',position:'absolute'});

//       var liCont = $targetLi.length;

//       if(effectFilter == 'fade') {
//         $setList.css({display:'block',opacity:'0',zIndex:'98'}).stop().css({'opacity':0}).addClass('showlist2');
//         typeOn();

//         if(liCont > 1) {
//           setInterval(function(){
//             var $activeShow = $targetObj.find('.showlist2');
//             // $activeShow.animate({opacity:'0'},effectSpeed,easing,function(){
//               $(this).next().css({display:'block',opacity:'0',zIndex:'99'}).css({'opacity':0}).addClass('showlist2');
//               typeOn();
//               $(this).appendTo($targetUl).css({display:'none',zIndex:'98'}).removeClass('showlist2');
//             // });
//           },switchDelay);
//         }


//       } else if(effectFilter == 'roll') {
//         $setList.css({top:'3em',display:'block',opacity:'0',zIndex:'98'}).stop().animate({top:'0',opacity:'1'},effectSpeed,easing).addClass('showlist2');
//         if(liCont > 1) {
//           setInterval(function(){
//             var $activeShow = $targetObj.find('.showlist2');
//             $activeShow.animate({top:'-3em',opacity:'0'},effectSpeed,easing).next().css({top:'3em',display:'block',opacity:'0',zIndex:'99'}).animate({top:'0',opacity:'1'},effectSpeed,easing).addClass('showlist2').end().appendTo($targetUl).css({zIndex:'98'}).removeClass('showlist2');
//           },switchDelay);
//         }
//       } else if(effectFilter == 'slide') {
//         $setList.css({left:(ulWidth),display:'block',opacity:'0',zIndex:'98'}).stop().animate({left:'0',opacity:'1'},effectSpeed,easing).addClass('showlist2');
//         if(liCont > 1) {
//           setInterval(function(){
//             var $activeShow = $targetObj.find('.showlist2');
//             $activeShow.animate({left:(-(ulWidth)),opacity:'0'},effectSpeed,easing).next().css({left:(ulWidth),display:'block',opacity:'0',zIndex:'99'}).animate({left:'0',opacity:'1'},effectSpeed,easing).addClass('showlist2').end().appendTo($targetUl).css({zIndex:'98'}).removeClass('showlist2');
//           },switchDelay);
//         }
//       }
//     });
//   });
// });


/*******************************************************/
//popbox
$(window).load(function() {
    $('.popbox').popbox({
        'open'          : '.open',
        'box'           : '.box',
        'close'         : '.close'
    });
});

$(function(){

   $("#menu_button").click(function(){
      $("#g_nav").toggleClass( "open");
   });

});

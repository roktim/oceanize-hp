<?php
require("./functions.php");
$url = $_SERVER["REQUEST_URI"];

include_once("header.php");
?>

<div id="container">
<div id="containerWrap"></div>
	<div class="statementBox">
        
		<div id="delay_image00" style="display:none;">
			<img class="statement_readImage sp_none" src="./img/statement_readImage.png" >
			<img class="display_img pc_none" src="./img/statement_readImage_sp.png">
		</div>
		<div class="statement_content">
			<div id="delay_image01">
				<img class="statement_text n1_sp pc_none" src="img/statement-01.png" >
                <img class="statement_text n1 sp_none" src="img/statement_text1.png" >
			</div>
			<div id="delay_image02">
				<img class="statement_text n2_sp pc_none" src="img/statement-02.png">
                <img class="statement_text n2 sp_none" src="img/statement_text2.png">
			</div>
			<div id="delay_image03">
				<img class="statement_text n3_sp pc_none" src="img/statement-03.png">
                <img class="statement_text n3 sp_none" src="img/statement_text3.png">
			</div>
			<div id="delay_image04">
				<img class="statement_text n4_sp pc_none" src="img/statement-04.png">
                <img class="statement_text n4 sp_none" src="img/statement_text4.png">
			</div>
			<div id="delay_image05">
				<img class="statement_text n5_sp pc_none" src="img/statement-05.png">
                <img class="statement_text n5 sp_none" src="img/statement_text5.png">
			</div>
			<div id="delay_image06">
				<img class="statement_text n6_sp pc_none" src="img/statement-06.png">
                                <img class="statement_text n6 sp_none" src="img/statement_text6.png">
			</div>
			<div id="delay_image07">
                            <img class="statement_underText_sp pc_none" src="img/statement-07.png" alt="私たち、オーシャナイズは学生を動かすことで、世界を動かしていく。">
                            <img class="statement_underText sp_none"  src="img/statement_underText.png" alt="私たち、オーシャナイズは学生を動かすことで、世界を動かしていく。">
							<h1 style="display:none;">私たち、オーシャナイズは、学生を動かすことで、世界を動かしていく。</h1>
			</div>
			<div  id="delay_image08">
                            <img class="logoRead_text_sp pc_none" src="img/statement-08.png">
                            <span class="sp_none logoRead_text" style="margin-top: 10px; font-size: 22px !important;letter-spacing: 0.075em;">Students Ark</span>
                            <!--Students Ark-->
                            <img class="statement_logoRead_sp pc_none" style="margin-top: -13px !important" src="img/statement_underLogo.png" >
                            <img class="statement_logoRead sp_none"  src="img/statement_underLogo.png">
			</div>
		</div>
	</div>
</div>




<?php include_once("footer.php");?>
<?php include_once("analyticstracking.php") ?>
</body>
</html>
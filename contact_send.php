<?php

session_start();

$type   = $_POST["type"];
$name   = $_POST["name"];
$mail   = $_POST["mail"];
$tel    = $_POST["tel"];
$textContent = $_POST["textContent"];
$referer     = $_POST["referer"];

if(empty($referer)) $referer = "ホームページ";

//==========================================================================
//メールmsg
$msg = "{$name}様から";
$msg.="お問い合わせが届きました。\n";


//メールBODY
$body = <<<BODY
===  お問い合わせ内容  ===
【氏名】  {$name} \n\r
【電話番号】 {$tel} \n\r
【メールアドレス】 {$mail} \n\r
【種別】 {$type} \n\r
【参照元】 {$referer} \n\r
【お問い合わせ内容】 \n\r
{$textContent} \n\r
 \n\r

BODY;



//メールfotter
$footer  = "━━━━━━━━━━━━━━━━━━━━━━━━━━━\n";
$footer .= "株式会社オーシャナイズ\n\n";
$footer .= "〒106-0031\n";
$footer .= "東京都港区西麻布3-22-10 StudentsArk\n";
$footer .= "t 03-6432-9221\n";
$footer .= "f 03-6447-4594\n";
$footer .= "【コーポレートサイト】    ：http://oceanize.co.jp\n";
$footer .= "【タダコピ】              ：http://www.tadacopy.com/\n";
$footer .= "━━━━━━━━━━━━━━━━━━━━━━━━━━━\n";


//自動返信用
//$mail_to = $m_mail;
if ($type == "広告主") {
    $subject = "【info】広告出稿-クライアント";
} elseif ($type == "広告代理店") {
    $subject = "【info】広告出稿-広告代理店";
} elseif ($type == "教育機関") {
    $subject = "【info】設置-教育機関";
} elseif ($type == "学生団体の方") {
    $subject = "【info】学生団体";
} else {
    $subject = "【info】その他";
}
$header     = "From:information@oceanize.co.jp";

mb_language("Japanese");
$oc_to      = "webmaster@oceanize.co.jp";
if ($type == "学生団体の方") $oc_to = "student_support@oceanize.co.jp";
$oc_subject = $subject;
$oc_header    =$header;
//OC向けに送信
mb_language("Japanese");
$body    = str_replace("\n", "", $body);
$oc_body    =   $msg . $body . $footer; //bodの組み立て
//メール送信終わり

/*
 * NEW SEND MAIL
 */

include('mailer.php');

$mailler            = new Mailer();
$mailler->From      = "information@oceanize.co.jp ";
$mailler->FromName  = $oc_header;
$mailler->AddAddress($oc_to);
$mailler->AddReplyTo($mail);

$mailler->IsHTML(false);                                  // set email format to HTML

$mailler->Subject   = $oc_subject; //"Here is the subject";
$mailler->Body      = $oc_body; //"This is the HTML message body <b>in bold!</b>";


if (!$mailler->Send()) {
    echo "<script type= 'text/javascript'>
    	alert(\"Message could not be sent. \");
    		</script>";
    exit();
}


//お客様返信メールmsg
$msg = "{$name}様\n\n";
$msg.=" この度は、お問い合わせ頂きまして、\n";
$msg.="誠にありがとうございます。\n\n";
$msg .= "お問合せいただいた内容に関しましては、\n担当の者からご連絡させていただきます。\n";
$msg.="\n\n※このメールは自動で送信しております。\n\n";


//メールBODY
$body = <<<BODY
===  お問い合わせ内容  ===
【氏名】  {$name} \n\r
【電話番号】 {$tel} \n\r
【メールアドレス】 {$mail} \n\r
【種別】 {$type} \n\r
【お問い合わせ内容】 \n\r
{$textContent} \n\r
 \n\r

BODY;



//メールfotter
$footer  = "━━━━━━━━━━━━━━━━━━━━━━━━━━━\n";
$footer .= "株式会社オーシャナイズ\n\n";
$footer .= "〒106-0031\n";
$footer .= "東京都港区西麻布3-22-10 StudentsArk\n";
$footer .= "t 03-6432-9221\n";
$footer .= "f 03-6447-4594\n";
$footer .= "【コーポレートサイト】    ：http://oceanize.co.jp\n";
$footer .= "【タダコピ】              ：http://www.tadacopy.com/\n";
$footer .= "━━━━━━━━━━━━━━━━━━━━━━━━━━━\n";


//自動返信用
//$mail_to = $m_mail;

$header     = "From:information@oceanize.co.jp";
$subject = "お問い合わせ自動返信";
mb_language("Japanese");
$oc_to      = $mail;
$oc_subject = $subject;
$oc_header    =$header;
//お客様向けに送信
$oc_body    =   $msg . $body . $footer; //bodの組み立て
//メール送信終わり

/*
 * NEW SEND MAIL
*/

$mailler            = new Mailer();
$mailler->From      = "information@oceanize.co.jp ";
$mailler->FromName  = $oc_header;
$mailler->AddAddress($oc_to);
$mailler->AddReplyTo("information@oceanize.co.jp ");

$mailler->IsHTML(false);                                  // set email format to HTML

$mailler->Subject   = $oc_subject; //"Here is the subject";
$mailler->Body      = $oc_body; //"This is the HTML message body <b>in bold!</b>";


if (!$mailler->Send()) {
	echo "<script type= 'text/javascript'>  alert(\"Message could not be sent. \");</script>";
	exit();
}


//logの生成------------------------------------------
$csv_base_url = "/var/www/logs/";
if ($type == "広告主") {
    $file = $csv_base_url . "log_client_cl.csv";
} elseif ($type == "広告代理店") {
    $file = $csv_base_url . "log_client_ag.csv";
} elseif ($type == "教育機関") {
    $file = $csv_base_url . "log_setti.csv";
} else {
    $file = $csv_base_url . "log_etc.csv";
}

$fp = fopen($file, "a");

$log_time = date("Y/m/d H:i:s");
flock($fp, LOCK_EX);
fwrite($fp, $log_time);
fwrite($fp, ",");
fwrite($fp, $name);
fwrite($fp, ",");
fwrite($fp, $tel);
fwrite($fp, ",");
fwrite($fp, $mail);
fwrite($fp, ",");
fwrite($fp, $type);
fwrite($fp, ",");
fwrite($fp, str_replace("\r\n", "", $textContent));
fwrite($fp, "\n");
flock($fp, LOCK_UN);
fclose($fp);

session_unset();

//$_SESSION['success'] = "お問い合わせ、ありがとうございました。";

echo "<script type= 'text/javascript'> parent.thankPop(); </script>";
exit();
?>

<!-- Google Code for B&#21521;&#12369;LP&#21839;&#12356;&#21512;&#12431;&#12379; Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 989796804;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "JwQTCI_e-2cQxLP81wM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/989796804/?label=JwQTCI_e-2cQxLP81wM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

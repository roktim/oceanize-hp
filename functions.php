<?php

/*******************************************************/
//XSS対策
function h($s) {
	return htmlspecialchars($s, ENT_QUOTES, 'UTF-8');
}

function e($t) {
	echo mb_convert_encoding($t, "utf-8");
}

//開発関数

/*******************************************************/
//整形var_dump();
function d($v) {
    echo '<pre style="background:#fff;color:#333;border:1px solid #ccc;margin:2px;padding:4px;font-family:monospace;font-size:12px">';
    foreach (func_get_args() as $v) var_dump($v);
    echo '</pre>';
  }

// 文字数抜粋
  function excerpt60($w) {
  	$strlen = mb_strlen($w, "utf-8");
  	if ($strlen > 60) {
  		echo mb_substr($w, 0, 59, "utf-8"). "…";
  	} else {
  		echo $w;
  	}
  }
/*******************************************************/

  function excerpt($u) {
  	$strlen = mb_strlen($u, "utf-8");
  	if ($strlen > 13) {
  		echo mb_substr($u, 0, 12, "utf-8"). "…";
  	} else {
  		echo $u;
  	}
  }







?>

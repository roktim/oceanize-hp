<?php
//require("./functions.php");
//$url = $_SERVER["REQUEST_URI"];
header('Location: https://recruit.oceanize.co.jp');
exit();
//include_once("header.php");
?>

<div id="container">
    <div id="containerWrap"></div>
    <div class="recruitBox">
        <img class="recruit_readImage pc" src="./img/recruit_readImage.png" style="margin-top:4px">
        <img class="recruit_readImage mobile" src="./img/rec_sma14@3x.png" >
        <div class="recruit_headline mobile" >
            <img src="./img/rec_sma02@3x.png">
        </div>
        <div class="recruit_headline pc">
            <img src="./img/pc_recruit/pc_recruit_06.png" >
        </div>
        <div class="recruit_headline_2 mobile">
            <img src="./img/rec_sma03@3x.png">
        </div>
        <div class="recruit_headline_2 pc">
            <!-- <img src="./img/recruit-02.png" style="margin: -16px 0 0 -3; width:487px" > -->
            <!--			以下の4つから、適性やご希望に応じて業務を担当していただきます。-->
        </div>
        <h1 style="display: none;">オーシャナイズという船をいっしょに動かしてくれるクルーを募集しています。</h1>


        <div class="recruit_contentTitle n1">
            <img class="recruit_readImage1" src="./img/pc_recruit/pc_recruit_09.png">
        </div>
        <div class="recruit_content mobile">
            <img class="recruit_contentImage1" src="./img/rec_sma05@3x.png">
        </div>
        <div class="recruit_content pc">
            <img class="recruit_contentImage1" src="./img/pc_recruit/pc_recruit_10.png">
        </div>
        <!--
                <div class="recruit_content">
                        航海士は、オーシャナイズという船全体の指揮をとる仕事です。<br />
                        安全な航海が実行できるよう、事前にスケジュールを確認し、<br />
                        船内の仲間と一緒に計画を練ります。<br />
                        ＞＞社内での業務は、一部上場企業をはじめとした、数多くのクライアントの<br class="sp_none" />
                        営業を行っていただきます。社内の仲間と協力をして、ニーズに沿った提案をしていきます。
                </div>
        -->
        <div class="recruit_contentTitle n2">
            <img class="recruit_readImage2" src="./img/pc_recruit/pc_recruit_12.png">
        </div>
        <div class="recruit_content mobile">
            <img class="recruit_contentImage2" src="./img/rec_sma07@3x.png">
        </div>
        <div class="recruit_content pc">
            <img class="recruit_contentImage2" src="./img/pc_recruit/pc_recruit_15.png">
        </div>
        <div class="recruit_contentTitle n3">
            <img class="recruit_readImage3" src="./img/pc_recruit/pc_recruit_18.png">
        </div>
        <div class="recruit_content mobile">
            <img class="recruit_contentImage3" src="./img/rec_sma09@3x.png">
        </div>
        <div class="recruit_content pc">
            <img class="recruit_contentImage3" src="./img/pc_recruit/pc_recruit_21.png">
        </div>
        <div class="recruit_contentTitle n4">
            <img class="recruit_readImage4" src="./img/pc_recruit/pc_recruit_24.png">
        </div>
        <div class="recruit_content mobile">
            <img class="recruit_contentImage4" src="./img/rec_sma11@3x.png">
        </div>
        <div class="recruit_content pc">
            <img class="recruit_contentImage4" src="./img/pc_recruit/pc_recruit_27.png">
        </div>
        <a class="recruit_link" href="./application.php">
            <img class="recruit_linkImage mobile" src="./img/rec_sma12@3x.png">
            <img class="recruit_linkImage pc" src="./img/pc_recruit/pc_recruit_30.png">
        </a>
    </div>










</div>




<?php include_once("footer.php"); ?>
<?php include_once("analyticstracking.php") ?>
</body>
</html>
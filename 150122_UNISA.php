<?php
require("./functions.php");
$url = $_SERVER["REQUEST_URI"];

include_once("header.php");
?>

<div id="container">
    <div id="containerWrap"></div>
    <div class="unisaBox">
        <div class="unisa_title">
            NEWS
        </div>

        <div>報道関係者各位</div>

        <div class="unisa_date" >
            2015年00月00日<br/>
            株式会社オーシャナイズ
        </div>

        <div class="unisa_headline" style="clear: both">
            オーシャナイズ、ＳＭＢＣ日興証券と提携し<br/>
            「大学生への金融リテラシー普及プロジェクト」を始動
        </div>

        <div class="unisa_content">
            <div class="unisa_paragraph_indent">
                2014年７月9日（水）に品川ステラボール（会場規模：約2000人）にて弊社が主催したイベント、 『大学対抗
                女子大生アイドル日本一決定戦“UNIDOL2014 Summer“』がニュースサイト約150媒体に紹介されました。
            </div>
            <div class="unisa_item">
                <img class="unisa_content n1" src="img/unisa_item1.png" />
                UNIDOL(ユニドル)とはUniversity Idolを由来としており、早稲田大学、上智大学、 明治大学、青山学院大学
                など、各大学に所属する「アイドルコピーダンスサークル」が出場する日本初となる大学対抗アイドルダンスコ
                ンテストです。
            </div>
        </div>
        <div class="unisa_content">
            <div class="unisa_paragraph_indent">
                UNIDOL(ユニドル)は、年々増加するアイドル業界に伴い増加する、 アイドルが大好きでたまらない女子大
                生達にスポットをあて、本気で「アイドル」として舞台に立って頂きます。
            </div>
            <div class="unisa_paragraph_indent">
                各チームのアマチュアとは思えない高いレベルのパフォーマンスや、アイドルが大好きだという熱意が生み
                だすドラマや感動が魅力となり規模を毎年あげております。
            </div>
        </div>
        <div class="unisa_content">
            <div class="unisa_paragraph_indent">
                2012年に第一回大会を開催して以来、毎度規模を拡大し、昨年12月にはSHIBUYA AXにて約1500名、今
                回大会では品川ステラボールにて約2000人を動員いたしました。
            </div>
            <div class="unisa_item"><img class="unisa_content n2" src="img/unisa_item2.png" /></div>
            日程：2014年7月9日（水）<br />
            <div class="unisa_left_indent">
                第1部（敗者復活戦）：16:00～<br />
                第2部（決勝）　　 ：18:15～
            </div>
            会場：品川ステラボール<br />
            <div class="unisa_left_indent">
                参加チーム：28組（25大学）<br />
                動員数　　：2011名（招待・関係者/出演者込）
            </div>
        </div>
    </div>
</div>

<?php include_once("footer.php"); ?>
<?php include_once("analyticstracking.php") ?></body>
</html>
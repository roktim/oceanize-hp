<?php
require("./functions.php");
$url = $_SERVER["REQUEST_URI"];

include_once("header.php");
?>

<div id="container" class="bussness-container">
    <div id="containerWrap"></div>
    <div class="businessBox">
        <div class="business_headline">
           <img class="sp_none" src="img/pc_business/pc_bus_03_160816.png" style="max-width:115.25%;">
            <!-- <p class="business_title font-special">オーシャナイズは、大学生（若者）との</p>
            <p class="btitle_new font-special">新しいコミュニケーションを創造する専門家集団です。</p> -->
           <img class="pc_none" src="img/sp_business/title_160823.png" width="306px">
                <div class="business_headline_text pc_none sp_none">
                    <h1>
                        私たち、オーシャナイズは全国200以上のキャンパスに
                        タダコピのインフラを持ち、自社のサービスによる25万人の
                        大学生会員データベースを保有し、400団体以上の
                        学生団体・サークルとのネットワークがあります。
                        全国大学生協連・私立学校厚生事業連合会と協業し、
                        600社を超える企業との大学生向け企画実施の実績がある
                        大学生コミュニケーションの専門家集団です。
                    </h1>
                </div>
            <img class="sp_none business_header_img" src="img/pc_business/pc_bus_04_160816.png">
        </div>
        <div class="row business_groups sp_none">
            <div class="row parts">
                <div class="part-left"><img src="img/pc_business/group_1_1.png" alt="Group One"></div>
                <div class="part-right"><img src="img/pc_business/group_1_2.png" alt="Group Two"></div>
                <!-- /.part -->
            </div>
            <!-- /.row -->
            <div class="row parts">
                <div class="part-left"><img src="img/pc_business/group_1_3.png" alt="Group Three"></div>
                <div class="part-right"><img src="img/pc_business/group_1_4.png" alt="Group Four"></div>
                <!-- /.part -->
            </div>
            <!-- /.row -->
            <div class="row parts parts-double">
                <div class="part-full">
                    <img src="img/pc_business/group_1_5.png" alt="">
                </div>
                <!-- /.part-full -->
            </div>
            <!-- /.row parts -->

<!--             <div style="margin:43px auto 95px auto; width:100%;">
	            <a href="http://business.smart-campus.jp/index.php#" target="_blank">
	            	<img src="img/pc_business/pc_bus_12_160816.png" alt="" style="margin-left: 253px; margin-top: -3px; width: 38.5%;">
	            </a>
            </div> -->

            <a href="http://business.smart-campus.jp/index.php#" target="_blank">
                <div class="business_button">
                </div>
            </a>

             <img class="sp_none business_sub_header_img" src="img/pc_business/pc_bus_13_160816.png">


            <div class="row parts">
                <div class="part-left"><img src="img/pc_business/group_2_1.png" alt="Group One"></div>
                <div class="part-right"><img src="img/pc_business/group_2_2.png" alt="Group Two"></div>
                <!-- /.part -->
            </div>
            <!-- /.row -->
            <div class="row parts">
                <div class="part-left"><img src="img/pc_business/group_2_3.png" alt="Group Three"></div>
                <div class="part-right"><img src="img/pc_business/group_2_4.png" alt="Group Four"></div>
                <!-- /.part -->
            </div>

            <!-- <div style="margin: 29px auto 32px auto; width:100%;">
            	<a href="./senpai/" target="_blank">
            		<img src="img/pc_business/pc_bus_12_160816.png" alt="" style="width: 38.5%; margin-left: 24.3%;">
            	</a>
            </div> -->

            <a href="http://smartschool.oceanize.co.jp" target="_blank">
                <div class="business_button" style="
    margin: 27px 0px 32px 253px;">
                </div>
            </a>

            <!--
            <a href="http://oceanize.co.jp/senpai/" target="_blank">
                <div class="business_button" style="
    margin: 27px 0px 32px 253px;">
                </div>
            </a>
            -->

        </div>
        <!-- /.business_section -->

         <div class="business_contentsWrap pc_none" style="margin-top:20px;margin-bottom:10px;">
			<img class="business_content" src="img/sp_business/title_1_160823.png">
        </div>

        <div class="business_groups pc_none">
            <div class="parts">
                <!-- <div class="part"><img src="img/business_group_1.png" alt="Group One"></div>
                <div class="part"><img src="img/business_group_2.png" alt="Group Two"></div>
                <div class="part"><img src="img/business_group_3.png" alt="Group Three"></div>
                <div class="part"><img src="img/business_group_4.png" alt="Group Four"></div>
                <div class="part"><img src="img/business_group_5.png" alt="Group Five"></div> -->
                <img src="img/sp_business/prof_1.png" alt="Group One" />
                <img src="img/sp_business/prof_2_160823.png" alt="Group Two" />
                <img src="img/sp_business/prof_3.png" alt="Group Three" />
                <img src="img/sp_business/prof_4.png" alt="Group Four" />
                <img src="img/sp_business/prof_5.png" alt="Group Five" />
                <!-- /.part -->
            </div>
            <!-- /.parts -->
        </div>

        <div class="pc_none" style="text-align:center;margin-top:15px;margin-left:auto;margin-right:auto;max-width:271px;">
        	<a href="http://business.smart-campus.jp/index.php#" target="_blank">
                <img class="business_content" src="img/sp_business/button_160823.png" style="width:271px;">
            </a>
        </div>

        <div class="business_contentsWrap pc_none" style="margin-top:40px;margin-bottom:20px;">
			<img class="business_content" src="img/sp_business/title_2_160823.png">
        </div>

        <div class="business_contentsWrap pc_none" style="margin-top:10px;margin-bottom:10px;">
			<img class="business_content" src="img/sp_business/sec_1_160823.png">
        </div>

        <div class="business_contentsWrap pc_none" style="margin-top:10px;margin-bottom:10px;">
			<img class="business_content" src="img/sp_business/sec_2_160823.png">
        </div>

        <div class="business_contentsWrap pc_none" style="margin-top:10px;margin-bottom:10px;">
			<img class="business_content" src="img/sp_business/sec_3_160823.png">
        </div>

        <div class="business_contentsWrap pc_none" style="margin-top:10px;margin-bottom:10px;">
			<img class="business_content" src="img/sp_business/sec_4_160823.png">
        </div>

        <div class="pc_none" style="text-align:center;margin:15px auto 60px auto;max-width:271px;">
        <!--
            <a href="http://oceanize.co.jp/senpai/" target="_blank">
                <img class="business_content" src="img/sp_business/button_160823.png" style="width:271px;">
            </a>
        -->
            <a href="http://smartschool.oceanize.co.jp" target="_blank">
                <img class="business_content" src="img/sp_business/button_160823.png" style="width:271px;">
            </a>
        </div>
        <script>
            if (screen.width < 640) {
                $('.business_contentsBox.right').removeClass('right').addClass('left');
            }
        </script>

    </div>
</div>




<?php include_once("footer.php"); ?>
<?php include_once("analyticstracking.php") ?></body>
</html>

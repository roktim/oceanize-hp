<?php
 ini_set('display_errors',0);
session_start(); ?>
<html lang="ja" prefix="og: http://ogp.me/ns#">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
        <meta charset="UTF-8">
        <title><?php



            // $testWrap = './img/pc_index.jpg';
            $pageId = str_replace('.php', '', end(explode('/', $url)));
            if (empty($pageId)) {
                $pageId = 'index';
                echo "タダコピ・学生マーケティング・大学広告・集客 |オーシャナイズ";
            } elseif (strstr($url, 'index')) {
                $pageId = 'index';
                echo "タダコピ・学生マーケティング・大学広告・集客 |オーシャナイズ";
            } elseif (strstr($url, 'statement')) {
                $testWrap = './img/sma_7.jpg';
//                $testWrap = './img/pc_statement.jpg';
                echo "タダコピ・学生・起業・夢・世界 |オーシャナイズ";
            } elseif (strstr($url, 'business')) {
                //$testWrap = './img/sma_3.jpg';
                $testWrap = './img/pc_business.jpg';
                echo "タダコピ・学生・大学・教育・広告・サンプリング・集客 |オーシャナイズ";
            } elseif (strstr($url, 'company')) {
                //$testWrap = './img/sma_5.jpg';
                // $testWrap = './img/pc_company.jpg';
                echo "学生集客・ 大学広告の専門家・タダコピ |オーシャナイズ";
            } elseif (strstr($url, 'recruit')) {
                //$testWrap = './img/sma_8.jpg';
                $testWrap = './img/pc_recruit/pc_recruit.jpg';
                echo "大学広告営業・大学向け営業・デザイナー・エンジニア・大学生インターン |オーシャナイズ";
            } elseif (strstr($url, 'contact')) {
                // $testWrap = './img/sma_6.jpg';
                $testWrap = './img/pc_contact.png';
                echo "大学広告主・広告代理店・教育機関の方 |オーシャナイズ";
            } elseif (strstr($url, 'application')) {
                $testWrap = './img/pc_apprication.jpg';
                echo "大学広告デザイン・学生集客・学生インターン・エンジニア募集 |オーシャナイズ";
            } elseif (strstr($url, 'performance')) {
                $testWrap = '';
                echo "ニュース・メディア掲載 |オーシャナイズ";
            } elseif (strstr($url, 'indexSp')) {
                // $testWrap = './img/sma_2.jpg';
            }
            $testWrap = '';
            ?>
        </title>
        <meta property="og:title" content="株式会社オーシャナイズ" />
        <meta property="og:locale" content="ja_JP" />
		<meta property="og:type" content="website" />
		<meta property="og:url" content="http://oceanize.co.jp/" />
		<meta property="og:image" content="http://oceanize.co.jp/img/siteLogo.jpg" />

        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=yes">
        <meta name="description" content="
            <?php
            if ($pageId == 'index') {
                echo "大学生向け広告メディア、タダコピを運営する学生向けマーケティング専門会社オーシャナイズです。
                大学生向けのマーケティングリサーチ（座談会）から、サンプリングやミスコンキャスティング、就活イベント集客まで幅広く対応が可能です。";
            } elseif (strstr($url, 'statement')) {
                echo "大学生向けのマーケティング会社として、私たちオーシャナイズは、学生を動かすことで世界を動かしていく。";
            } elseif (strstr($url, 'business')) {
                echo "タダコピを運営するオーシャナイズは
                大学生向けの座談会などのリサーチ業務から、マーケティングプラン立案実行、就活イベント運営・企画・集客まで幅広く対応可能です。
                オンライン広告メディアや学生に人気のアプリも企画・開発・運営しております。";
            } elseif (strstr($url, 'company')) {
                echo "大学生向け広告メディア、タダコピを運営する学生向けマーケティング専門会社オーシャナイズのアクセスはこちら！";
            } elseif (strstr($url, 'recruit')) {
                echo "大学生向け広告メディア、タダコピを運営する学生向けマーケティング専門会社オーシャナイズでは、
                学生向けの事業やメディア企画・開発、デザインを担えるメンバーを募集しております！";
            } elseif (strstr($url, 'contact')) {
                echo "学生向けのマーケティング、集客、開発のご相談はこちらからお願い致します。
                大学生向けマーケティングは、全国どこでも対応可能です！";
            } elseif (strstr($url, 'performance')) {
                echo "大学生向け広告メディア、タダコピを運営する学生向けマーケティング専門会社オーシャナイズのメディア掲載実績や最新のニュースです！";
            } elseif (strstr($url, 'application')) {
                echo "学生マーケティングのオーシャナイズは採用を積極的に行っております。
                こちらから応募をお願い致します。";
            }
            ?>
        ">
        <meta name="keywords" content="タダコピ,広告媒体,紙媒体,平面媒体,広告,大学,大学生向け,無料,タダ,０円,コピー,印刷，裏面">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="Cache-Control" content="no-cache">
        <meta http-equiv="Expires" content="0">
        <meta name="google-site-verification" content="O-y5_rDOc3MZEKRsSGeYbBs30nX6GGe_8ftc8JcI0PM" />
        <link id="favicon" rel="shortcut icon" href="img/favicon.ico" />
<!--        <link rel="stylesheet" href="css/loaders.min.css">-->
        <link id="css" rel="stylesheet" href="css/style.css?1703281">


        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
        <script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=true"></script>

<!--        <script type="text/javascript" src="js/loaders.css.js"></script>-->
        <script type="text/javascript" src="./js/script.js"></script>
        <script type="text/javascript" src="./js/jquery.screwdefaultbuttonsV2.js"></script>
        <script type="text/javascript" src="./js/fastclick.js"></script>
        <script src="js/jquery.transform.js"></script>
        <script src="js/popbox.js" type="text/javascript"></script>

        <script>
            // 画面サイズが小さい場合はstyle_sp
            if (screen.width < 668) {
                $('#css').replaceWith('<link rel="stylesheet" href="./css/style_sp.css?0924">');
            }
        </script>
    </head>



    <?php if (strstr($url, 'statement') || strstr($url, 'recruit')): ?>
        <!-- 反転版 -->
        <body class="invert" id="<?php echo $pageId;?>">
            <div class="page_loader">
                <div id="floatingCirclesG">
                    <div class="f_circleG" id="frotateG_01"></div>
                    <div class="f_circleG" id="frotateG_02"></div>
                    <div class="f_circleG" id="frotateG_03"></div>
                    <div class="f_circleG" id="frotateG_04"></div>
                    <div class="f_circleG" id="frotateG_05"></div>
                    <div class="f_circleG" id="frotateG_06"></div>
                    <div class="f_circleG" id="frotateG_07"></div>
                    <div class="f_circleG" id="frotateG_08"></div>
                </div>
            </div>
            <header>
                <?php if (!empty($testWrap)): ?>
                    <!-- testWrap -->
                    <div class="testWrap">
                        <img src="<?php echo $testWrap ?>" alt="">
                    </div>
                <?php endif ?>
                <div class="headerContents">
                    <a href="./index.php">
                        <img class="siteLogo" src="./img/siteLogo2.png" alt="oceanize">
                    </a>
                    <div class="headerText invert">
                        Oceanize Tokyo : StudentsArk, 3-22-10,Nishiazabu,MInato-ku, Tokyo 106-0031,Japan<br />
                        Oceanize Vietnam : 7F TS Building No.17 Street No 2 , Cu Xa Do Thanh, Ward 4, District 3, Ho Chi Minh City<br />
                        Oceanize Bangladesh : Flat A3, House 257, Road 19/A, New DOHS, Mohakhali, Dhaka-1206, Bangladesh
                    </div>
                    <div class="tellNumber">
                        TEL：03-6432-9221<br class="sp_none" />
                        <span class="faxNumber">FAX：03-6447-4594</span>
                    </div>

                </div>
            </header>



        <?php else: ?>
            <!-- 通常版 -->
        <body onload="initialize(), initialize2()" class="normal" id="<?php echo $pageId?>">
            <div class="page_loader">
                <div id="floatingCirclesG">
                    <div class="f_circleG" id="frotateG_01"></div>
                    <div class="f_circleG" id="frotateG_02"></div>
                    <div class="f_circleG" id="frotateG_03"></div>
                    <div class="f_circleG" id="frotateG_04"></div>
                    <div class="f_circleG" id="frotateG_05"></div>
                    <div class="f_circleG" id="frotateG_06"></div>
                    <div class="f_circleG" id="frotateG_07"></div>
                    <div class="f_circleG" id="frotateG_08"></div>
                </div>
            </div>
            <!-- /.page_loader -->
            <div class="popWrap"></div>
            <header>
                <?php if (!empty($testWrap)): ?>
                    <!-- testWrap -->
                    <div class="testWrap">
                        <img src="<?php echo $testWrap ?>" alt="">
                    </div>
                <?php endif ?>
                <div class="headerContents">
                    <a href="./index.php">
                        <img class="siteLogo" src="./img/siteLogo.png" alt="oceanize">
                    </a>
                    <div class="headerText">
                        Oceanize Tokyo : StudentsArk, 3-22-10,Nishiazabu,MInato-ku, Tokyo 106-0031,Japan<br />
                        Oceanize Vietnam : 7F TS Building No.17 Street No 2 , Cu Xa Do Thanh, Ward 4, District 3, Ho Chi Minh City<br />
                        Oceanize Bangladesh : Flat A3, House 257, Road 19/A, New DOHS, Mohakhali, Dhaka-1206, Bangladesh
                    </div>
                    <div class="tellNumber">
                        TEL：03-6432-9221<br class="sp_none" />
                        <span class="faxNumber">FAX：03-6447-4594</span>
                    </div>
                    <!--
                    <div class="pc_none">
                        <div>
                            <button id="menuSelect">
                                <div class="menu_icon">
                                    <img class="menu_P" id="p01" src="img/menu_p.png">
                                    <img class="menu_P" id="p02" src="img/menu_p.png">
                                    <img class="menu_P" id="p03" src="img/menu_p.png">
                                </div>
                            </button>
                        </div>
                        <ul>
                            <li><a href="statement.php">PHILOSOPHY</a></li>
                            <li><a href="business.php">BUSINESS DOMAINS</a></li>
                            <li><a href="http://lab.oceanize.co.jp/" target="_blank">STUDENTS LAB</a></li>
                            <li><a href="company.php">COMPANY</a></li>
                            <li><a href="recruit.php">CAREERS</a></li>
                            <li><a href="contact.php">CONTACT</a></li>
                        </ul>
                    </div>
                    -->
                </div>
            </header>


        <?php endif; ?>

        <div class="pc_none">
            <nav id="g_nav">
                <div id="menu_button">
                    <span class="nav_icon"></span>
                    <small class="menu_txt">MENU</small>
                </div>
                <ul class="clearfix">
                    <li><a href="statement.php">PHILOSOPHY</a></li>
                    <li><a href="business.php">BUSINESS DOMAINS</a></li>
                    <li><a href="http://lab.oceanize.co.jp/" target="_blank">STUDENTS LAB</a></li>
                    <li><a href="company.php">COMPANY</a></li>
                    <li><a href="https://recruit.oceanize.co.jp/" target="_blank">CAREERS</a></li>
                    <li><a href="contact.php">CONTACT</a></li>
                </ul>
            </nav>
        </div>
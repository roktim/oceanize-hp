<?php

require("./functions.php");
$url = $_SERVER["REQUEST_URI"];

include_once("header.php");
?>


<div id="container">
<!-- お問い合わせへのレスポンス -->
  <?php if (isset($_SESSION['success'])): ?>
    <div class="popbox" onClick="sessionSuccess();">
      <div class='collapse'>
        <div class='box sessionSuccess'>
            お問い合わせ、ありがとうございました。
        </div>
      </div>
    </div>
    <?php unset($_SESSION['success']); ?>
  <?php endif; ?>

  <div class="indexBox">

    <div class="top_readImage">
      <img class="top_readImage" src="./img/top_readImage.png" alt="oceanize">
      <a class="top_learn_more" href="./statement.php">
        learn more
        <img src="./img/link_icon.png">
      </a>
    </div>

    <div class="index_headline">
        <h1>
            私たち、オーシャナイズは<br class="pc_none" />学生を動かすことで、世界を動かしていく。
        </h1>
    </div>
    <!-- Begin row 1 -->
    <div class="index_contentsBoxTop">
      <div class="index_content Num1" >
        <div class="popbox">
<!--          <img src="./img/tadacopy_logo.png" class='open' alt="タダコピ">-->
          <img class="open" src="./img/logos/logo-1.png" alt="タダコピ">
          <div class="index_ContentText open n1">
            Copy for free.<br />
            Be free to enjoy campus life.<br />
            service:tadacopy  since:2005
          </div>
          <div class='collapse'>
              <div class='box'>
                  <a class="popLogo popup-1" target="_blank" href="http://www.tadacopy.com/clients.html#b1">
<!--                    <img src="img/tadacopyPop_logo.png" alt="タダコピ">-->
                      <img src="img/popup_logos/TC-18_03.png" alt="タダコピ">
                  </a>
                  <a class="popText popup-1-txt" target="_blank" href="http://www.tadacopy.com/clients.html#b1">
                    <img src="img/tadacopyPop_text.png" alt="">
                  </a>
                  <div class="close r15">
                    <img src="img/tadacopyPop_icon.png" alt="">
                  </div>
                  <img class="popbanner" src="img/tadacopyPop_sp.png" alt="">
              </div>
            </div>
        </div>
      </div>

      <div class='index_content Num2' >
        <div class='popbox'>
<!--          <img src="./img/tadach_logo.png" class='open' alt="タダch">-->
          <img class="open" src="./img/logos/logo-2.png" alt="タダch">
          <div class="index_ContentText open n2">
            Expore your new world.<br />
            Real-time broadcast for Students.<br />
            service:tadacopy channel  since:2014
          </div>
          <div class='collapse'>
              <div class='box'>
                  <a class="popLogo popup-2" target="_blank" href="http://www.tadacopy.com/clients.html#b4">
<!--                    <img src="img/tadachPop_logo.png" alt="タダch">-->
                      <img  src="img/popup_logos/popup-logo-2.png" alt="タダch">
                  </a>
                  <a class="popText" target="_blank" href="http://www.tadacopy.com/clients.html#b4">
                    <img style="width:30%;" src="img/tadachPop_text.png" alt="">
                  </a>
                  <div class="close">
                    <img src="img/tadachPop_icon.png" alt="">
                  </div>
                  <img class="popbanner" src="img/tadachPop_sp.png" alt="">
              </div>
            </div>
          </div>
        </div>
    </div>
    <!--End row 1 -->

    <!-- Begin row  2-->
    <div class="index_contentsBoxTop">
      <div class='index_content Num3'>
        <div class='popbox'>
<!--          <img src="./img/tadacopy2_logo.png" class="open" alt="タダコピ">-->
            <img class="open" src="./img/logos/logo-3.png" alt="タダコピ">
          <div class="index_ContentText open n3">
            Fun to use tadacopy.<br />
            It is a students hub.<br />
            service:tadacopy app  since:2014
          </div>
          <div class='collapse'>
            <div class='box'>

                <a class="popLogo popup-3" target="_blank" href="http://www.tadacopy.com/app/">
<!--                  <img src="img/tadacopy2Pop_logo.png" alt="タダコピ">-->
                    <img  src="img/popup_logos/popup-logo-3.png" alt="タダコピ">
                </a>
                <a class="popText" target="_blank" href="http://www.tadacopy.com/app/">
                  <img src="img/tadacopy2Pop_text.png" alt="">
                </a>
                <div class="close r15">
                  <img src="img/tadacopy2Pop_icon.png" alt="">
                </div>
                <img class="popbanner" src="img/tadacopy2Pop_sp.png" alt="">
            </div>
          </div>
          </div>
      </div>

     <div class='index_content Num4'>
       <div class="popbox">
          <img src="./img/unidol_logo.png" class="open" alt="UNIDOL">
          <div class="index_ContentText open n4">
            Who is your favorite!?<br />
            Japanese idols in University is here!
            service:unidol  since:2014
          </div>
          <div class='collapse'>
            <div class='box'>

                <a class="popLogo" target="_blank" href="http://unidol.jp/">
                  <img src="img/unidolPop_logo.png" alt="UNIDOL">
                </a>
                <a class="popText" target="_blank" href="http://unidol.jp/">
                  <img src="img/unidolPop_text.png" alt="">
                </a>
                <div class="close">
                  <img src="img/unidolPop_icon.png" alt="">
                </div>
                <img class="popbanner" src="img/unidolPop_sp.png" alt="">
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- End row 2-->

    <!-- Begin row 3-->
    <div class="index_contentsBoxTop">

      <div class="index_content Num5" >
        <div class="popbox">
          <img src="./img/toretan_logo.png" class="open" alt="トレタン">
          <div class="index_ContentText open n5">
            Study Hub for classroom.<br />
            Share tips save time,geting smart.<br />
            service:toretan!  since:2014
          </div>
          <div class='collapse'>
            <div class='box'>

                <a class="popLogo" target="_blank" href="https://toretan.jp/">
                  <img src="img/toretanPop_logo.png" alt="トレタン">
                </a>
                <a class="popText" target="_blank" href="https://toretan.jp/">
                  <img src="img/toretanPop_text.png" alt="">
                </a>
                <div class="close r15">
                  <img src="img/toretanPop_icon.png" alt="">
                </div>
                <img class="popbanner" src="img/toretanPop_sp.png" alt="">
            </div>
          </div>
        </div>
      </div>

      <div class="index_content Num6" >
        <div class="popbox">
<!--          <img src="./img/canpass_logo.png" class="open" alt="CANPASS">-->
            <img class="open" src="./img/logos/logo-6.png" alt="CANPASS">
          <div class="index_ContentText open n6">
            Get sweet deals & coupon.<br />
            Campus life is so sweet!<br />
            service:can>pass since:2014
          </div>
          <div class='collapse'>
            <div class='box'>

                <a class="popLogo popup-6" target="_blank" href="http://canpassapp.com/">
<!--                  <img src="img/canpassPop_logo.png" alt="CANPASS">-->
                    <img src="img/popup_logos/popup-logo-6.png" alt="CANPASS">
                </a>
                <a class="popText" target="_blank" href="http://canpassapp.com/">
<!--                  <img src="img/canpassPop_text.png" alt="">-->
                    <img style="width:25%" src="img/popup_logos/logo-6-txt.png" alt="">
                </a>
                <div class="close">
                  <img src="img/canpassPop_icon.png" alt="">
                </div>
                <img class="popbanner" src="img/canpassPop_sp.png" alt="">
            </div>
          </div>
        </div>
      </div>

    </div>
    <!-- End row 3-->

    <!-- Begin row4-->
    <div class="index_contentsBoxTop">
      <div class="index_content Num7" >
        <div class="popbox">
<!--          <img src="./img/magazine_logo.png" class="open" alt="MAGAZINE">-->
            <img class="open" src="./img/logos/logo-7.png" alt="MAGAZINE">
          <div class="index_ContentText open n7">
            Written by students, for students.<br />
            Community for great campus life.<br />
            service:MAGAZINE  since:2015
          </div>
          <div class='collapse'>
            <div class='box'>

                <a class="popLogo popup-7" target="_blank" href="http://mag.canpassapp.com/">
<!--                  <img src="img/magazinePop_logo.png" alt="MAGAZINE">-->
                    <img src="img/popup_logos/magazin_logo.png" alt="">
                </a>
                <a class="popText" target="_blank" href="http://mag.canpassapp.com/">
<!--                  <img src="img/magazinePop_text.png" alt="">-->
                    <img style="width:40%" src="img/popup_logos/magazon_txt.png" alt="">
                </a>
                <div class="close r15">
                  <img src="img/magazinePop_icon.png" alt="">
                </div>
                <img class="popbanner" src="img/magazinePop_sp.png" alt="">
            </div>
          </div>
        </div>
      </div>

      <div class="index_content Num8">
        <div class="popbox">
          <img src="./img/capture_logo.png" class="open" alt="Capture">
          <!-- <div style="font-size: 21px;margin: 6px 0 8px -4px;" class="open" >Capture</div> -->
          <div class="index_ContentText open n8">
            Capture the moment,<br />
            get your dream job.<br />
            service:Capture  since:2015
          </div>
          <div class='collapse'>
            <div class='box'>

                <div class="popLogo" target="_blank" href="">
<!--                  <img src="img/capturePop_logo.png" alt="Capture">-->
<!--                  <img src="img/capturePop_logo_sp.png" alt="Capture" class="capture_sp"> -->
                  <img src="img/popup_logos/logo_8_sp.png" alt="Capture" class="capture_sp">
                    <!-- When release app ==> remove class 'capture_sp' and remove _sp in src of image-->
                </div>
                <div class="popText" target="_blank" href="">
                  <!-- <a href="http://capture-news.jp/lp"> -->
                  <img src="img/capturePop_text.png" alt="">
                  <!-- </a> -->
                </div>
                <div class="close">
                  <img src="img/capturePop_icon.png" alt="">
                </div>
                <img class="popbanner" src="img/capturePop_sp.png" alt="">
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- End row 4-->

    <!-- Begin row 5-->
    <div class="index_contentsBoxTop">
      <div class="index_content Num9">
        <div class="popbox">
          <img src="./img/teppan_logo.png" class="open" alt="TEPPAN">
          <!-- <div style="font-size: 21px;margin: 12px 0 8px 10px" class="open">TEMPLE</div> -->
          <div class="index_ContentText open n9">
            Simplicity and efficiency<br />
            email app for student job seekers.<br />
            service:TEPPAN  since:2015
          </div>
          <div class='collapse'>
            <div class='box'>
                <a class="popLogo" target="_blank" href="http://teppan.email/lp">
                  <img src="img/teppanPop_logo.png" alt="" class="TEPPAN">
                </a>
                <a class="popText" target="_blank" href="http://teppan.email/lp">
                  <img src="img/teppanPop_text.png" alt="" >
                </a>
                <div class="close r15">
                  <img src="img/teppanPop_icon.png" alt="">
                </div>
                 <img class="popbanner" src="img/teppanPop_sp.png" alt="">
            </div>
          </div>
        </div>
      </div>
      <div class="index_content Num10">
        <div class="popbox">
          <img src="./img/senpai_logo.png" class="open" alt="senpai">
          <!-- <div style="font-size: 21px;margin: 6px 0 8px -3px;" class="open">SENPAI</div> -->
          <div class="index_ContentText open n10">
            under construction<br />
            coming soon...<br />
            service:SENPAI since:2015
          </div>
          <div class='collapse'>
            <div class='box'>

                <div class="popLogo" target="_blank" href="">
<!--                  <img src="img/senpaiPop_logo.png" alt="senpai">-->
<!--                  <img src="img/senpaiPop_logo_sp.png" alt="senpai" class="senpai_sp">-->
                  <img src="img/popup_logos/logo_10_sp.png" alt="senpai" class="senpai_sp">
                    <!-- When release app ==> remove class 'senpai_sp' and remove _sp in src of image-->
                    <span class="comming_soon_sp"><img src="img/popup_logos/comming_soon_sp.png" alt=""></span>
                </div>
                <div class="popText senpai_text" target="_blank" href="">
                  <img src="img/senpaiPop_text.png" alt="">
                </div>
                <div class="close">
                  <img src="img/senpaiPop_icon.png" alt="">
                </div>
                <img class="popbanner" src="img/senpaiPop_sp.png" alt="">
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- End row 5-->

    <div class="slideBox">
      <div class="ticker" rel="fade">
        <div class="tickerTitle">
          NEWS
        </div>
        <ul>
        <li class="tgt"><a href="./performance.php">2017.8.2　株式会社スマートライフとの資本業務提携のお知らせ
          </a></li>
        <li class="tgt"><a href="https://recruit.oceanize.co.jp/" target="_blank">2017.7.4　採用ページをリニューアルいたしました。
          </a></li>
        <li class="tgt"><a href="./performance.php">2017.6.26　中高生向け授業動画サービス「MANAVIE」β版をリリースしました！
          </a></li>
          <li class="tgt"><a href="./performance.php">2017.6.19　SmartSchool麻布本校主催　小学生向け「社会とつながるサマースクール」を実施します！
          </a></li>
          <li class="tgt"><a href="./performance.php">2017.5.1　自社運営教室「SmartSchool 麻布本校」開校！
          </a></li>
        <li class="tgt"><a href="./performance.php">2017.4.13　北九州市立大学・北方キャンパスにタダコピ登場！
          </a></li>
        <li class="tgt"><a href="./performance.php">2017.4.13　北九州市立大学・ひびきのキャンパスにタダコピ登場！
          </a></li>
        <li class="tgt"><a href="./company.php">2017.3.24　オフィスを移転いたしました。
          </a></li>
        <li class="tgt"><a href="./performance.php">2017.3.3　大阪教育大学・柏原キャンパスにタダコピ登場！
          </a></li>
        <li class="tgt"><a href="./performance.php">2016.8.1　写真共有アプリ『PICON』事業譲受のお知らせ
          </a></li>
        <li class="tgt"><a href="./performance.php">2016.7.19　大東文化大学・東松山キャンパスにタダコピ登場！
          </a></li>
        <li class="tgt"><a href="./performance.php">2016.6.24　岡山理科大学にタダコピ登場！
          </a></li>
        </ul>
      </div><!--/.ticker-->

      <div class="ticker2" rel="fade">
        <div class="tickerTitle">
          MEDIA
        </div>
        <ul>
        	<li class="tgt2"><a href="./performance.php">2017.9.6　大学に広がる無料コピー機「タダコピ」の記事が、
          	産経新聞、産経ニュースに掲載されました。</a></li>
            <li class="tgt2"><a href="./performance.php">2015.3.14　女子大生アイドル日本一決定戦「UNIDOL」の記事が、
              朝日新聞、朝日新聞DIGITALに掲載されました。</a></li>
              <li class="tgt2"><a href="./performance.php">2015.3.11　学生の金融リテラシー向上を目指すＳＭＢＣ日興証券様との共同プロジェクト
                「ＵＮＩＳＡ（ユニーサ）」がメディアに掲載されました。</a></li>
                <li class="tgt2"><a href="./performance.php">2014.10.8　四月一日企画・シブヤテレビジョンと共同で行った、
                  LINEクリエイタースタンプの人気投票結果が掲載されました。</a></li>
                  <li class="tgt2"><a href="./performance.php">2014.9.15　日本経済新聞にてタダコピが紹介されました。</a></li>
                  <li class="tgt2"><a href="./performance.php">2014.7.28　「UNIDOL(ユニドル)」が各種ニュースサイトで紹介されました。</a></li>
        </ul>
      </div><!--/.ticker2-->
    </div>




  </div>
</div>




<?php include_once("footer.php");?>
<?php include_once("analyticstracking.php") ?></body>
</html>